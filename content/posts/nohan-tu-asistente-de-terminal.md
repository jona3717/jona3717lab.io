---
title: "Nohan Tu Asistente De Terminal"
date: 2020-07-20T12:13:28-03:00
draft: false
toc: false
image: "https://jona3717.gitlab.io/images/nohan-info.png"
tags:
  - terminal
  - asistente
  - consola
  - nohan
  - asistente de terminal
  - linux
  - gnu
  - gnu/linux
  - aplicacion
  - programa
  - jona3717
---

{{< image src="/images/nohan-info.png" style="border-radius: 3px;" >}}

Una de las cosas que más me han gustado siempre son los asistentes virtuales y en Linux hay unos cuantos, de los que destaco a Sarah de SemiCode OS. Lamentablemente el proyecto no sigue en pie pero si ves lo que ofrecía Sarah, seguro quedas encantado. Era un asistente para la terminal, que podía dar información de películas, abrir programas y mucho más. En esta ocación vengo a contarte de algo similar, no es un asistente tan completo como Sarah, sino más bien algo más sencillo, es un asistente que puede ayudarte con tareas sencillas como realizar búsquedas en la web y actualizar tu distribución.

### Sobre el nombre

NoHAN es el acrónimo de "Not Have A Name", en español "No tener un nombre". ¿Por qué? bueno, a decir verdad no andaba muy inspirado al momento de nombrar a mi asistente, así que al no tener un nombre que ponerle, pensé en las siglas y me gustó como sonaba.

### Sobre NoHAN

Es mi primer proyecto desarrollado en Python, por lo que si ves el código, me disculpo por el posible desorden. Aunque he tratado de mantenerlo lo más presentable posible con las últimas actualizaciones.

{{< image src="/images/nohan-krunner.png" style="border-radius: 3px;" >}}

Como dije, está desarrollado en Python, y también posee algunas características desarrolladas con PyQt, la librería de Python para interfaces gráficas con el framework Qt. NoHAN está pensado para trabajar con el Krunner de Plasma, sin embargo, algunos de los comandos, deben realizarse desde una terminal, y como podrán deducir, también es posible utilizarlo desde cualquier terminal. El desarrollo ha sido enfocado a distribuciones basadas en Arch y Debian, por lo que si se instala en otra distribución, podrían no funcionar algunas característicash como la actualización del sistema.

NoHAN cuenta con una licencia GPLv3.

### Instalación

El proyecto está alojado en Gitlab, por lo que para instalarlo, solo debes clonarlo desde el repositorio.
```
git clone https://gitlab.com/jona3717/nohan/
```
Una vez descargado, tendrás la carpeta nohan, solo debes entrar al directorio y ejecutar el archivo `instalar` para comenzar el procesode instalación. Este asistente se vale de algunos paquetes adicionales para funcionar correctamente, por lo que te pedirá la contraseña para realizar la instalación.

```
cd nohan
./instalar
```

### ¿Cómo usarlo?

Para ver una lista completa con los parámetros que le puedes pasar a nohan, basta con pedirlo:

`nohan ayuda`

Ahora solo tienes que probar cada uno de los comandos y disfrutar de NoHAN.

**Nota:** Si bien es cierto, que este asistente cuenta con la función de reconocimiento de voz, aún se encuentra en desarrollo y puede presentar algunos conflictos para reconocer correctamente las palabras. Por lo que puedes probarlo, pero no se asegura una funcionalidad al 100%. El reconocimiento de voz utiliza la api de Google. 

Si tienes alguna recomendación, duda, sugerencia o comentario, no dudes en contactarme a través de mis redes sociales que puedes encontrar en la columna de la derecha o si lo prefieres, enviándome un mensaje a [mi correo.](mailto:jona3717@protonmail.com?subject=A%20tu%20entrada%20sobre%20NoHan)

{{< image src="/images/nohan-ayuda.png" style="border-radius: 3px;" >}}

No olvides compartir.
