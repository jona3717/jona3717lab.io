---
title: "¿Qué pasó con Telltale Games?"
date: 2023-06-02T00:46:21-03:00
draft: false
toc: false
images:
tags:
  - telltale games
  - telltale
  - games
  - juegos
  - compañia videojuegos
  - videojuegos
---

{{< image src="/images/telltalegames.jpg" style="border-radius: 3px;" >}}
Imagen por [Caballero de la tormenta](https://hiepsibaotap.com/wp-content/uploads/2020/04/telltale-games-generacion-xbox.jpg)
##

Telltale era una empresa estadounidense que desarrollaba videojuegos, especializada en las aventuras gráficas episódicas. Ha adaptado varias franquicias famosas. La compañía se fundo en 2004 por antiguos empleados de LucasArts y tuvo su primer gran éxito con Sam & Max: Season One en 2006. Sin embargo, se declaró en banca rota en septiembre de 2018 luego de 14 años. El cierre causó que más de 200 personas quedaran sin trabajo y la cancelación de todos los títulos en los que trabajaban. El cierre fue causado por distintos factores, entre ellos la falta de ventas y la cancelación de proyectos importantes. En la compañía ya habían problemas financieros desde hace algún tiempo antes de cerrar y habían despedido a varios de sus empleados en noviembre de 2017.
Cuando la compañía no pudo más y cerró sus puertas, los empleados fueron despedidos sin previo aviso. Según un artículo en Polygon, los empleados no recibieron indemnización por despido y perdieron sus beneficios de salud. Algunos empleados se unieron a otros estudios de videojuegos como Skybound Games y otros fundaron AdHoc Studios y contrataron a algunos de sus ex compañeros de trabajo.

## Catálogo de Juegos
La empresa se hizo famosa por juegos como The Walking Dead, Tales from the Borderlands y The wolf among us. Aunque esos son solo tres de los más de cien juegos que desarrolló, incluyendo secuelas. Hagamos un breve repaso de estos juegos:

- Telltale Texas Hold'Em
      > Es un juego de póquer lanzado en 2005. Fue el primer juego lanzado por la compañía y se desarrolló originalmente como una plataforma de prueba para el motor de Telltale. El juego presenta un torneo de póquer contra cuatro personajes con personalidades únicas. Está disponible en Steam para Windows.

- Bone: The Great Cow Race y Out From Boneville
      > Basado en la serie de cómics Bone de Jeff Smith, sigue las aventuras de Fone Bone en su primera entrega y a los primos Fone, Phoney y Smiley Bone. Los juegos están disponibles en Steam para Windows.

- CSI
      > Una serie de juegos desarrollados para Ubisoft en torno a CSI. Estos juegos incluyen CSI: 3 Dimensions of Murder, Hard Evidence, Deadly Intent y Fatal Conspiracy. Cada uno se lanzó de forma individual.

- Sam & Max
      > Una serie de juegos de aventuras gráficas que sigue las aventuras de Sam, un perro detective y Max, un conejo "híperkinético" y "violento". Los juegos se centran en la resolución de casos y en la exploración de los entornos.

- Wallace and Gromit's
      > Una saga que cuenta con un total de siete juegos para Xbox 360, PC, PlayStation 2, GameCube, GameBoy Advance y Xbox, que siguen las aventuras de Wallace, un inventor tonto y su perro Gromit. Al igual que en el caso de Sam y Max, los juegos se centran en la resolución de casos y en la exploración de los entornos.

- Tales of Monkey Island
      > Una aventura gráfica desarrollada en colaboración con LucasArts para Windows, MacOS y la consola Wii. El juego fue desarrollado bajo licencia de LucasArts y marca la primera colaboración entre las dos empresas. El juego se centra en el personaje principal Guybrush Threepwood y su lucha contra el pirata zombie LeChuck. El juego consta de cinco episodios que se lanzaron entre Julio y Diciembre del 2009.

- Puzzle Agent
      > Un juego de aventuras ambientado en un pueblo rural de Minnesota en el que suceden cosas extrañas. Fue lanzado en 2010 y cuenta con una secuela llamada Puzzle Agent 2. El juego presenta una serie de rompecabezas que el jugador debe resolver para avanzar en la historia. El juego fue creado con la narrativa y sensibilidades visuales únicas del artista de cómics independiente Graham Annable y entregado con el estilo distintivo de narración de Telltale.

- Poker Night at The Inventory
      > Es un juego de póquer que presenta a personajes de videojuegos como Max de Sam & Max, Srong Bad de Homestar Runner, Tycho Brahe de Penny Arcade y el Heavy Weapons Guy de Team Fortress 2. El juego fue lanzado en 2010 y cuenta con una secuela llamada Poker Night 2. Presenta una serie de torneos de póquer que el jugador debe ganar para avanzar en la historia.

- Back to the Future
      > Basado en la trilogía de películas Back to the Future, presentan a Marty McFly y Doc Brown en una nueva aventura. Cuenta con cinco episodios y fue lanzado en 2010. Presenta una serie de rompecabezas que el jugador debe resolver para avanzar en la historia.

- Strong Bad's Cool Game for Attractive People
      > Una serie de cinco episodios de aventuras gráficas. El juego presenta al personaje Strong Bad, de la serie web Homestar Runner. Fue lanzado en 2008 y tal como en el título anterior, presenta una serie de rompecabezas que deberás completar si quieres avanzar en la historia.

- Hector
      > Es una serie de tres episodios de aventuras gráficas desarrollada por Straandlooper y publicada por Telltale Games. El juego presenta al detective Hector en su lucha contra el crimen en la ciudad ficticia de Clapper's Wreake y fue lanzado en 2011. Este es otro juego de rompecabezas.

- Law & Order: Legacies
      > Es una serie de siete episodios que presenta a personajes de la serie de TV Law & Order. El juego fue lanzado en 2011 una vez más es otro juego de rompecabezas.

- The Walking Dead: Esta entrega tiene cincuenta juegos
      > La serie principal cuenta con un total de cuatro temporadas de las cuales las tres primeras están formadas por cinco episodios y la última, de cuatro. Además de esta serie, lanzada para consolas y PC, hay muchos más juegos móviles de los que quisiera, un par de juegos para realidad virtual y muchos ports para diversos dispositivos. La primera temporada es una de mis favoritas y si no has probado alguno de estos juegos, no sé a qué estás esperando.

- The Wolf Among Us
      > Es una serie de cinco episodios de aventuras gráficas. El juego está basado en la serie de cómics Fables y presenta a personajes como Bigby Wolf y Snow White. Fue lanzado en 2013 y con este sería el enésimo juego de rompecabezas.

- Game of Thrones
      > Una serie de aventuras gráficas basado en la serie de TV Game of Thrones y presenta a personajes como Cersei Lannister y Tyrion Lannister. Fue lanzado en 2014 y cuenta con seis episodios. El juego también presenta una serie de rompecabezas que el jugador debe resolver para avanzar en la historia. ¿Qué se traían los de telltales con los rompecabezas?

- Tales from the Borderlands
      > Es una serie de cinco episodios de aventuras gráficas lanzada en 2014. Está basado en la serie de videojuegos Borderlands (que por cierto son juegazos) y presenta a personajes como Rhys y Fiona. Este es otro juego... Bueno, ya no lo diré más, pueden deducirlo ustedes mismos.

- Minecraft: Story Mode
      > Una serie de ocho episodios de aventuras gráficas lanzada en 2015 basado en el juego Minecraft. Presenta a personajes como Jesse y Petra. No diré más de qué va el juego si se trata de lo mismo. Ya estuvo con eso.

- Batman: The Telltale Series
      > Una serie de cinco episodios de aventuras gráficas lanzada en 2016. El juego está basado en el personaje de DC Comics, Batman y presenta a personajes como Bruce Wayne y Selina Kyle. ¿Qué les digo? Muy buen juego, si eres fan de Batman como yo, te lo recomiendo.

- Marvel's Guardians of the Galaxy
      > Es una serie de cinco episodios de aventuras gráficas lanzada en 2017. El juego está basado en los personajes del Universo Marvel, Guardianes de la Galaxia y presenta a personajes como Star Lord y Rocket Raccoon.

- Batman: The Enemy Within
      > Una serie de cinco episodios de aventuras gráficas lanzada en 2017. El juego está basado en el personaje de DC Comics Batman y presenta a personajes como Bruce Wayne y John Doe.

- RGX: Showdown
      > Un juego de carreras futurista uno contra uno. Fue lanzado para Xbox One y PlayStation 4 en 2018 y retirado en 2021 debido a las bajas ventas y la poca cantidad de jugadores.

- The Telltale Batman Shadows Edition
      > Es una colección que incluye los juegos Batman: The Telltale Series y Batman: The Enemy Within.

- The Expanse: A Telltale Series
      > Otro juego episódico de aventura narrativa al estilo Telltale que transcurre antes de los acontecimientos vistos en la serie de TV, The Expanse. Fue desarrollado con Deck Nine y está previsto para ser lanzado en PC y consolas (aún no confirmadas) a mediados del presente año. Y por si ves esto mucho tiempo después, estamos en 2023, de nada. El juego sigue a Cara Gee, quien repite el papel de Camina Drummer y explora los peligrosos confines del cinturón a bordo de la Artemis.

## Juegos cancelados

- Stranger Things
      > Un juego interactivo basado en la serie de Netflix, Stranger Things, desarrollado para PC y consolas. Estaba previsto para ser lanzado en 2019, pero fue cancelado en septiembre de 2018 debido al cierre del estudio. El juego iba a tener a Will Byers como el personaje principal y se iba a ambientar antes de la segunda temporada de la serie.

- The Wolf Among Us: Second Season
      > Fue anunciada en 2017 y se esperaba que saliera en 2018, sin embargo el juego fue cancelado en septiembre de 2018 debido al cierre por problemas financieros. El juego iba a continuar la historia de Bigby Wolf y los personajes de Fables. Contaría con cinco episodios, aunque en algún momento se pensó en reducirlos a tres. Se iba a usar el motor Unity en lugar del motor propio de Telltale. Se pueden encontrar algunos videos e imágenes del juego que muestran parte del arte conceptual y el prototipo del juego.

- Juego basado en saga de zombies diferente a The Walking Dead.
      > Tendría una narrativa procedural o aleatoria y permitiría al jugador crear sus propias historias.

- Game Of Thrones: Second Season
      > Fue confirmada en noviembre de 2015 por el fundador del estudio, Kevin Bruner. El juego iba a ser una secuela de la primera temporada, que se basaba en la serie de HBO, sin embargo, el juego fue cancelado en septiembre de 2018 por la misma causa que los anteriores. Tras la reestructuración del estudio bajo una nueva dirección, Telltale Games aclaró que ya no poseía la licencia de Game Of Thrones, al igual que otras muchas de sus franquicias.

- Juego basado en el universo de Indiana Jones
      > De hecho, esto fue un rumor que circuló en 2010, cuando se dijo que Telltale Games había adquirido los derechos de varias franquicias de LucasArts, entre ellas Indiana Jones. Sin embargo, el rumor resultó ser falso y Telltale Games solo obtuvo los derechos de Monkey Island y Sam & Max. Desde entonces no se ha anunciado ningún proyecto de Telltale Games relacionado con Indiana Jones.

## La compra de Telltale Games
En agosto de 2019, la empresa LCG Entertainment adquirió los activos de Telltale Games y relanzó la marca con un nuevo equipo de desarrollo. La nueva Telltale Games, mantendrá los derechos comerciales de varias de sus licencias y sagas, como Puzzle Agent y podrá volver a usar títulos como Batman y The Wolf Among Us. LCG Entertainment es un nuevo holding empresarial que se fundó en 2019 con el objetivo de recuperar la marca y algunas de sus licencias más populares. La nueva Telltale Games abrió una oficina en Malibú, California y contrató a algunos de los antiguos empleados del estudio original como autónomos. También se asoció con Athlon Games para distribuir sus nuevos juegos, que mantendrán el formato episódico pero con más interacción y opciones. Algunas de las sagas que la nueva Telltale Games no podrá seguir desarrollando son The Walking Dead o Stranger Things.

## Telltale Games en la actualidad

Actualmente, Telltale Games está desarrollando la segunda temporada de The Wolf Among Us y una serie basada en The Expanse.

-------------------------------------------------------------------------------

Decidí hablar sobre Telltale Games ya que es la compañía que dio vida a uno de mis juegos favoritos, The Walking Dead: Season One. Ahora que conoces algunas cosas sobre Telltale, me voy como va siendo habitual, con un par de preguntas: ¿Has jugado a alguno de los títulos desarrollados por esta compañía? ¿Cual es tu opinión sobre el juego o cuál ha sido tu favorito?
Ahora sí, hasta la próxima. Chau, chau. 🐧
