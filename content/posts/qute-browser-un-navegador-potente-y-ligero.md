---
title: "QuteBrowser Un Navegador Potente Y Ligero"
date: 2020-07-18T23:54:32-03:00
draft: false
toc: false
images:
tags:
  - navegador
  - browser
  - qutebrowser
  - navegador vim
  - navegador ligero
  - ligero
  - potente
---

{{< image src="/images/qutebrowser.png" style="border-radius: 3px;" >}}

## ¿Qué es QuteBrowser?

QuteBrowser es un navegador enfocado a su uso con el teclado. Este navegador está basado en Python y Pyqt5, es software libre bajo licencia GPL y tiene un toque del editor vim que se nota en su uso. Llevo ya un tiempo utilizando este navegador y debo decir que no me falta nada. Si bien no cuenta con sincronización, no es algo que me moleste. Lo que buscaba cuando llegué a este navegador era algo ligero y rápido, y qutebrowser lo es; además, cuenta con un addblocker incluido que no anda nada mal.

Soy un poco flojo y debo reconocerlo, me cuesta tener que sacar las manos del teclado para mover el cursor. Pero gracias a qutebrowser, esto está resuelto, ya que practicamente todo, puede hacerse sin necesidad del ratón.
## Instalación:

**Ubuntu**
```
sudo apt install qutebrowser
```
**Archlinux**
```
sudo pacman -S qutebrowser
```

{{< image src="/images/qutebrowser-ddg.png" style="border-radius: 3px;" >}}

Ahora que tienes instalado qutebrowser, puedes probarlo y disfrutar. Como opinión personal, puedo decir que me ayuda mucho, ya que aumenta mi productividad, me evita quitar las manos del teclado y me resulta muy ligero y veloz. Te toca probar y contar qué te pareció.

Te dejo esta imagen con los atajos de teclado y sus respectivas funciones, con las que puedes sacarle mucho provecho a este navegador.

{{< image src="/images/qutebrowser-shortcuts.png" style="border-radius: 3px;" >}}

### Algunos comandos:

- Abrir enlace: 			`:o`

- Abrir enlace en nueva pestaña:	`:O`

- Volver a página anterior:		`:H`

- Ir una página hacia adelante: 	`:J`

- Activar y actualizar addblock:	`:adblock-update`

- Mostrar la página de ayuda:		`:help`

{{< image src="/images/qutebrowser-yt.png" style="border-radius: 3px;" >}}

Espero que este tutorial haya sido de ayuda, me gustaría saber qué te pareció esta aplicación o si te animaste a instalarla.Puedes contactarme a mi email o a cualquiera de mis redes que puedes encontrar arriba en el panel derecho.

No olvides compartir.
