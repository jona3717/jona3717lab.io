---
title: "Actualizacion De Vivaldi Y El Uso De Mis Navegadores"
date: 2023-11-05T18:59:21-03:00
draft: false
toc: false
images:
tags:
  - navegadores
  - browser
  - blog
  - software
---

{{< image src="/images/navegadores-que-uso.jpg" style="border-radius: 3px;" >}}

Hace casi dos semanas, el navegador Vivaldi tuvo una actualización en la que incluía los "Espacios de trabajo" y además la noticia de su nuevo navegador para IOS. Este post, originalmente iba a ser un hilo en X (antes Twitter), pero al ver la extensión del texto me dije: Esto ya es demasiado, tienes un blog para estas cosas. Así que acá estamos una vez más.

Mi uso habitual no se restringe a un solo navegador, ya que utilizo la computadora para diferentes tareas. Debido a esto, tengo un navegador dedicado al desarrollo, otro a canales de difusión como YouTube, podcast, redes sociales, etc y otro para navegación aleatoria. Esto lo hago con el fin de no llenar un mismo navegador con decenas de pestañas de distintos temas.
Mi configuración está ajustada en: Abrir sesión anterior", es decir que cada vez que abro cualquier navegador, se vuelvan a mostrar las pestañas que deje abiertas la última vez. Así, cada navegador tiene siempre entre tres y seis pestañas abiertas que evito cerrar por el hecho de: "Cuando las necesite ahí estarán".

Ahora, retomando lo del primer post de este hilo: Vivaldi ha incluido en su actualización del 23 de Octubre, los espacios de trabajo. Es como tener varios navegadores en uno solo, ya que en cada espacio puedes tener las pestañas que quieras y estas no se mezclarán. Obviamente se le puede asignar un nombre a cada espacio y esto me evita tener que usar varios navegadores distintos. Sin embargo, debo decir que Vivaldi no es mi navegador por defecto (Actualmente, porque hubo un tiempo en que sí), ya que el navegador que utilizo en toda plataforma y en todo sitio es Firefox. No porque sea mejor, sino porque es el que mejor se adapta a mis necesidades. Los contenedores se volvieron indispensables en el uso diario y muy útiles. Además de la sincronización multiplataforma, que si bien es cierto que Vivaldi también la tiene, hasta hace poco en IOS aún no estaba disponible. Y no hay que olvidar del tremendo pack de seguridad que tenemos con Firefox. En fin, no soy un erudito de los navegadores, simplemente un usuario más y la experiencia que tengo hasta ahora es más que satisfactoria. Así que no creo que deje de utilizar más de un navegador, pero muy probablemente sí deje de usar cuatro. Sí damas y caballeros, uso cuatro. Y para que no queden huecos en esta historia, son los siguientes (sin ningún orden particular):

- [Firefox](https://www.mozilla.org/es-ES/firefox/new/)
- [Edge](https://www.microsoft.com/es-es/edge?form=MA13FJ)
- [Falcon](https://www.falkon.org/)
- [Vivaldi](https://vivaldi.com/es/)

Como dije antes, cada uno dedicado a un área diferente exclusiva en mi ordenador principal, porque en el trabajo uso Firefox y lo uso para todo, porque claro, nunca está de mas algo de música, lectura o algún video para distraerse en el trabajo. Pero ya que ahora surgió una nueva opción, habrá que probarla. No quiero decir que me quedare con Vivaldi gracias a sus espacios de trabajo porque no seria verdad, pero sí le daré la oportunidad para tener menos software instalado en mi sistema operativo. Aunque, siempre tuve una ventaja al usar tantos navegadores. La ventaja de probar distintos proyectos con distintas experiencias de usuario y disfrutar de sus características. Porque cada navegador, es diferente en muchos aspectos, tienen muchas características y, aunque no las exploto completamente (Sep, hablo del correo y calendario de Vivaldi, el Firefox View, las herramientas de Edge como su calculadora, cronómetro, convertidor de números, etc), son características que me gusta probar cuando se anuncian, que disfruto descubrir en cada actualización.

En fin, me he alargado ya más de lo necesario y seguro que me he enredado aún más; pero de eso iba el post y menos mal que no lo puse en un hilo... Mira nada más lo que habría sido eso. Hasta la próxima. 🐧
