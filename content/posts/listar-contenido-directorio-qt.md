---
title: "Listar el contenido de un directorio con Qt - QDir C++"
date: 2021-04-18T18:18:21-03:00
draft: false
tags:
  - programming
  - c++
  - cpp
  - programacion
  - programación
  - developer
  - develop
  - devel
  - c++ devel
  - c++ programming
  - programación c++
---

{{< image src="/images/qt.png" style="border-radius: 3px;" >}}

Ya he hablado sobre este framework en el blog, y esta vez les vengo a compartir una solución cuando se quiere listar el contenido de un directorio o carpeta usando Qt y C++.

Para esto haremos uso de la biblioteca QDir, y el tipo de dato QStringList.

```
QDir directorio("ruta/al/directorio");
QStringList elementos = directorio.entryList(QStringList() << "*.txt", QDir::Files);

foreach(QString nombrearchivo, elementos){
    // hacer lo que se quiera con cada archivo
}
```

Obviamente, para usar la biblioteca QDir tendremos que incluirla en nuestro archivo .cpp, para ello colocaremos la línea: `#include <QDir>` en la parte superior.
Ahora expliquemos un poco el fragmento de código anterior.

- En la primera línea, creamos un objeto de tipo QDir, que tendrá asignada la ruta del directorio que queremos listar.
- Luego creamos una QStringList llamada "elementos", que por lo que se puede leer, es una lista de QString, en la que almacenaremos todos los archivos que terminen en ".txt", esto quiere decir que listaremos solo los archivos de texto en nuestro directorio. Por último, le decimos el tipo de archivos que va a listar con QDir::Files. Podemos optar por otro tipo como Drives, System, entre otros más que podemos encontrar en la [documentación de Qt](https://doc.qt.io/qt-5/qdir.html#Filter-enum).
- Ahora solo queda iterar la lista elementos para mostrar los nombres de los archivos encontrados o agregarlos en algun widget de nuestro programa.

Espero que te haya sido de ayuda, no olvides compartir.🐧
