---
title: "Hacer un backup completo del sistema: ¿Cómo y qué herramientas usar? GNU/Linux"
date: 2022-10-10T23:08:22-03:00
draft: false
toc: false
images:
tags:
  - gnu/linux
  - gnu
  - linux
  - backup
  - tar
  - dd
---
{{< image src="/images/backup.jpg" style="border-radius: 3px;" >}}
Foto de [Kelsy Gagnebin](https://unsplash.com/@kelsymichael?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
 en [Unsplash](https://unsplash.com/es?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)

### Introducción:

Nota: Si no quieres saber el por qué de este post o no te es relevante, sáltate la introducción.

Hace un par de días, se me ocurrió cambiar el sistema de archivos de mi sistema de **ext4** a **btrfs,** pero no quería tener que reinstalar mi sistema y mucho menos todo el software que tengo instalado y configurado, por lo que hice un backup de mis datos previamente. Ahora es cuando viene la razón de este post; al hacer la copia de seguridad, usé un programa que siempre uso, pero no pensé en el resultado por lo que tuve muchas complicaciones. Al hacer la copia de respaldo no solo copié mis datos, sino que hice una imagen de mi sistema, el cual tenía en una partición con el sistema de archivos ext4 y al restaurarlo en una partición btrfs, bueno, ya se imaginan el lío que armé. En fin, todo tiene solución, así que se pudo resolver y ahora les dejo este post para que no tengan que pasar por lo que yo. (Más detalles de este fail pronto en el Podcast Jona3717).

### ¿Qué herramientas usaremos?

Hay muchas para lo que hoy nos compete, sin embargo yo uso dos que me resultan muy prácticas, confiables y sencillas:

- dd
- tar

### ¿Qué tipo de backup queremos hacer?

Para seleccionar la herramienta adecuada para nuestro backup, tenemos que saber para qué lo usaremos, es decir, ¿lo queremos solo para respaldar nuestros archivos o queremos tener una copia que nos facilite restaurar nuestro sistema por si se daña? (o lo dañamos nosotros, porque somos linuxeros y es lo que hacemos).

Si solo necesitamos nuestros archivos a buen resguardo, usaremos la utilidad tar, la típica herramienta para empaquetar o comprimir. Por otro lado, si lo que queremos es una imagen de nuestro sistema para poder restaurarlo o clonarlo en caso de necesitarlo, usaremos dd; esa herramienta con la que también podemos crear nuestro live usb.

Pero, hay algo más que debemos tener en cuenta:

La imagen de sistema realizada con dd  solo puede ser restaurada en una partición con el mismo sistema de archivos, es decir, si nuestra partición tiene ext4, la partición en la que restauremos nuestros datos deberá ser ext4.

El backup de sistema realizado con tar, conservará nuestros archivos (permisos incluidos) pero no el sistema de archivos, por ello podemos decir que no es una imagen del sistema, sino una copia de respaldo.

### Listando discos y particiones:

Es necesario hacer esto para saber cual es el nombre de la partición o del disco al que queremos respaldar:

```bash
sudo fdisk -l
```

### Haciendo un backup con dd:

```bash
sudo dd if=/dev/xxxx of=/ruta/de/backup.img conv=noerror status=progress
```

Veamos que hace el comando anterior:

- **sudo dd:** Ejecuta dd con permisos de superusuario.
- **if=:** Indicamos a qué partición le haremos la copia de seguridad
- **of=:** Indicamos la ruta en la que se guardará nuestra partición
- **conv=noerror:** Esto hará que la copia de nuestros archivos no se detenga aunque hayan errores de lectura.
- **status=progress:** Esto nos irá mostrando el progreso de la copia de datos.

### Haciendo un backup con tar:

```bash
sudo tar cvpzf /ruta/del/backup.tar.gz / --exclude=/proc --exclude=/lost+found --exclude=/mnt --exclude=/sys --exclude=dev/pts /ruta/a/respaldar
```

Veamos lo que hace el comando anterior:

- `sudo tar:` ejecuta tar con permisos de superusuario.
- `cvpzf`: Son las opciones que le pasamos a tar para indicarle qué queremos hacer.
    - `c:` Crea un nuevo archivo
    - `v`: Hace referencia a “verbose”, muestra el proceso a medida que se realiza.
    - `p`: Guarda los permisos de los archivos.
    - `z`: comprime el archivo con gzip
    - `f`: Selecciona los datos de la ruta que le indicamos a continuación.
- `--exclude=`: Esto hace que obviemos algunas carpetas y archivos que no son indispensables para nuestra copia de seguridad y que podrían causar problemas en el proceso. De este mono, evitamos tener un backup tan pesado.

Nota importante: Si el backup lo vamos a guardar en la misma ruta que vamos a respaldar o en el mismo dispositivo, es necesario agregar lo siguiente a nuestro comando:`--exclude=/ruta/de/backup`. Si el backup se encuentra dentro de una carpeta, bastará con excluir la carpeta, si está en la raíz de la ruta a respaldar, hay que indicar el nombre completo del backup incluyendo el formato de archivo: `.tar.gz`.

### Restaurar las copias de seguridad

Ahora la parte más sencilla:

**Con dd:**

```bash
sudo dd if=/ruta/del/backup.img of=/dev/xxx conv=noerror status=progress
```

**Con tar:**

```bash
sudo tar -zxvpf /ruta/del/backup.tar.gz /ruta/en/donde/se/restaura
```

Espero que esta entrada te haya sido de ayuda. Recuerda si quieres ayudarme con compartir estarás haciendo mucho. 🐧
