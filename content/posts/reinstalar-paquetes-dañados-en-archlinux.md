---
title: "Reinstalar Paquetes Dañados en Archlinux"
date: 2023-04-01T01:07:11-03:00
draft: false
toc: false
images:
tags:
  - gnu
  - linux
  - gnu linux
  - archlinux
  - arch
  - linux tips
  - tips
  - linux triks
  - triks
  - trucos
---

{{< image src="/images/boxes.jpg" style="border-radius: 3px;" >}}
Image by [Mediamodifier](https://pixabay.com/users/mediamodifier-1567646/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2108029) from [Pixabay](https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2108029)</a>

## ¿Paquetes de sistema rotos?
Nunca se me rompió el sistema con alguna actualización, pero con orgullo puedo decir que como buen linuxero he roto muchas veces mi sistema por trastear con él. Esto me ha hecho aprender mucho y reparar los paquetes dañados, es una de esas cosas que aprendí en el proceso. Si por alguna razón te ha pasado lo mismo, te dejo aquí un pequeño pero útil articulo.

## ¿Qué hacer luego de entrar en pánico al ver nuestro sistema roto?

Antes de empezar, recomiendo hacer esto con un live USB. Es posible hacerlo dentro del sistema, pero para evitar cualquier percance, usar un live USB nos hará más fácil la tarea de reinstalar todo si fallamos en el proceso. Claro que esa no es la idea.

1. Listamos todos los paquetes que tenemos instalados en nuestro sistema y guardamos la lista en un archivo de texto:

`sudo pacman -Qq > packages.text`

- `Q`<Query>: Nos permite consultar la base de datos de paquetes locales.
- `q` <quiet>: Muestra menos información sobre cada paquete listado, así solo nos muestra el nombre de cada paquete.

2. Creamos un script que haga el trabajo de reinstalar cada paquete uno por uno, yo usaré vim:

`vim reinstall.sh`
```
1.   #!/bin/bash
2.
3.   for package in $(cat packages.txt)
4.   do
5.       pacman -S $package --overwrite "*" --noconfirm
6.   done
```

- `Línea 1`: El hashbang o shebang que indica qué shell utilizar para ejecutar el script.
- `Línea 3`: Usamos un bucle for que itera en nuestra lista creada anteriormente.
- `Línea 4`: Palabra reservada indica que el código a continuación debe ser ejecutado por cada iteración.
- `Línea 5`: Comando que se ejecutará con cada paquete:
    - `$package`: Variable que será reemplazada por el nombre del paquete.
    - `--overwrite "*"`: Indica que debe sobrescribir la instalación anterior del paquete.
    - `--noconfirm`: No solicita confirmación de instalación por cada paquete
- `Línea 6`: Palabra reservada que indica el fin del bucle for.

3. Ejecutamos el script:

`./reinstall.sh`

Esto tardará dependiendo de la cantidad de paquetes instalados en tu distribución. Una vez que el script haya terminado su trabajo tendrás todos los paquetes reinstalados. Te recomiendo que una vez hecho esto, hagas un backup de tu sistema para que puedas restaurarlo si te sucede un accidente linuxero.

¡No olvides compartir! Chau chau. 🐧
