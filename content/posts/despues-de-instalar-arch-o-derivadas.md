---
title: "Después De Instalar Archlinux o Derivadas"
date: 2020-07-18T16:47:09-03:00
draft: false
toc: false
images:
tags:
  - gnu/linux
  - linux
  - tutorial
  - post install
---

{{< image src="/images/arch-postinstall.jpg" style="border-radius: 3px;" >}}

Esta entrada es más que nada una recomendación personal sobre algunas de las cosas que puedes hacer en tu distribución apenas instalada.

Repositorios
Actualizar y escoger los repositorios, son dos de las primeras cosas que se deben hacer; esto nos permitirá tener una mayor velocidad de descarga de los paquetes que queramos instalar. Para esto tenemos dos opciones

- Seleccionar los repositorios de nuestro país o los más cercanos.
- Utilizar reflector para que seleccione los repositorios por nosotros.

Acá te enseñaré como seleccionar los repositorios manualmente. Si deseas utilizar reflector, puedes instalarlo y dejar que él seleccione lo haga, puedes ver cómo usarlo aquí.

En una terminal, deberás abrir el archivo mirrorlist con el editor nano para seleccionar los repositorios de tu preferencia:
```
sudo nano /etc/pacman.d/mirrorlist
```
Una vez abierto el archivo, solo debes descomentar los repositorios que quieres utilizar, para esto solo hay que borrar las almohadillas "#". Ojo, solo debes descomentar las líneas que contengan enlaces, no las que indiquen el nombre del país de los repositorios.
Ahora solo queda guardar y salir del editor, para eso usaras la combinación de teclas <CTRL> + <X>, te preguntará si deseas guardar, solo pulsas la tecla <S> y luego en <ENTER>. Listo, ya tienes los repositorios seleccionados.

Ahora que ya has seleccionado los repositorios, solo debes actualizar. En la misma terminal utilizas el comando para actualizar el sistema:
```
sudo pacman -Syyu
```
Makepkg
Ahora los paquetes se descargan mucho más rápido, pero puedes mejorar aún más el trabajo de pacman acelerando el proceso de compilación de los paquetes. Para esto debes editar el archivo de configuración de makepkg de la siguiente manera:
Abre el archivo: **sudo nano /etc/makepkg.conf**

Busca la línea que contenga:
```
MAKEFLAGS="-j2"
```
Y reemplázala con:
```
MAKEFLAGS="-j($(nproc) + 1)"
```
Esto hará que utilice todos los núcleos del procesador para compilar los paquetes. la variable nproc indica el número de núcleos que posee el procesador.

Ahora debes buscar la siguiente línea, casi al final del archivo:

COMPRESSXZ=(xz -c -z)
Y reemplázala con:
```
COMPRESSXZ=(xz -c -z - --threads=0)
```
Listo, ya optimizaste el trabajo de pacman.

Instala un firewall
Nunca está de más decirlo, la seguridad es lo primero, y para ello existe Uncomplicated Firewall. Es un firewall ligero y además de muy simple uso, incluso hay un paquete que te ofrece la interfaz gráfica para que puedas usarlo sin tener que recurrir a la terminal.

**Instalación:**
```
sudo pacman -S gufw
```
Una vez instalado, solo hace falta activar su inicio junto al sistema e iniciarlo. en la misma terminal, deberás ejecutar lo siguiente:
```
sudo systemctl enable ufw sudo systemctl start ufw
```
Reiniciamos y con esto ya tenemos el firewall en marcha, para verificar ve al menú de aplicaciones y busca la configuración del cortafuegos, abre la aplicación y se mostrará una ventana como esta:

{{< image src="/images/gufw-inicio.png" style="border-radius: 3px" >}}

Ahora puedes crear reglas para permitir, denegar, etc.

**Adornar pacman**
Esto es algo que puede no darte ningún beneficio en cuanto al desempeño, pero sí hará que pacman se vea mucho mejor cuando lo utilicemos en la terminal, dándole color y una animación. Para esto debes editar el archivo de configuración de pacman.
Abre el archivo.
```
sudo nano /etc/pacman.conf
```
Descomenta la línea que contenga:
```
#Color
```
Debajo de la línea #VerbosePkgLists, agrega lo siguiente:
```
ILoveCandy
```
Y eso es todo por mi parte, si tienes alguna recomendación o si crees que hay algo que me ha faltado, no olvides que puedes decírmelo contactándome en mis redes en la columna de la derecha o enviándome un correo a: jona3717.protonmail.com

No olvides compartir.
