---
title: "Juegos Indie y por qué debes probarlos si aún no lo has hecho"
date: 2023-05-27T22:37:25-03:00
draft: false
toc: false
images:
tags:
  - juegos
  - indie
  - juegos indie
  - games
  - indie games
  - game
  - videojuegos
---

{{< image src="/images/juegos-indie.jpg" style="border-radius: 3px;" >}}
Imagen por [The Zeus Player](https://www.instagram.com/the_zeus_player/)

## ¿Qué son los juegos indie?

Una respuesta corta es: Son juegos independientes.
Pero, ¿qué quiere decir "Juegos independientes"? Pues ahí entra la respuesta larga.

Los juegos indie son desarrollados por pequeñas compañías, o equipos reducidos de personas. Y, son llamados independientes porque que no están sujetos a las pautas marcadas por una compañía. Estos juegos comparten algunas características como la originalidad y la innovación; y por lo general, son más cortos o tienen menos contenido que otros juegos (aunque esto no es del todo cierto). Al ser independientes no cuentan con apoyo financiero para su distribución, publicidad o financiación, por lo que usualmente el dinero es recaudado por *crowdfunding*, y la publicidad depende del eco que se hace por la comunidad de jugadores.

Ahora que conocemos el concepto de juegos indie, veamos algunas de las características principales de estos.

- Tienen independencia creativa
- Cuentan con un bajo presupuesto
- Suelen ser innovadores

Justamente es el bajo presupuesto lo que permite que sea posible arriesgarse a innovar y experimentar. Gracias a ello es que tenemos títulos como:

- Minecraft
- Project Zomboid
- Stardew Valley
- Fez

## ¿Cuál es la importancia de los juegos indie?
Como dije antes, es aquí en donde se permite el riesgo de innovar y experimentar. Esto hace posible aplicar nuevas mecánicas, estilos de arte, etc. Y es gracias a esto, que podemos disfrutar de juegos que no son más de lo mismo, en una industria en la que muchas veces nos encontramos con que la prioridad es el dinero y no el entretener o satisfacer al usuario.

## ¡Ahora atento!
Que aquí te dejo unos juegos para chuparse los dedos. Ah no, que eso era para la comida.

- **This War of Mine:** Empezaré por decir que no hay un personaje principal al que controlas, sino a un grupo de sobrevivientes. Es un videojuego crudo y tétrico en el que vives las consecuencias de la guerra. Deberás recorrer por distintos escenarios buscando recursos para mantener vivos a los integrantes de tu grupo, tomarás decisiones que serán cruciales para situaciones que podrán ser de vida o muerte. Situaciones como asesinar a un padre de familia para robarle la comida y sobrevivir un día más o no hacerlo y sacrificar a un miembro de tu grupo. Y hablando de muerte, si algún personaje muriese no esperes que vuelva a aparecer.

- **Blasphemous:** Un juego de plataformas ambientado en España, elementos de combate hack-n-slash, mucha acción y una ambientación maravillosa. Seguramente que más de una vez te pondrá los pelos de punta por la frustración ya que cada mínimo error se paga. La característica más destacada es el arte de este juego, construido en un *pixel art* tan cuidado que resulta fascinante.

- **Outer Wilds:** Un juego espacial de mundo abierto de acción y aventura que se centra en explorar un sistema solar atrapado en un bucle temporal infinito. Posee un diseño visual precioso, lo que hace que recorrer cada planeta no sea tedioso incluso si lo hacemos caminando, además al tener planetas pequeños, disfrutaremos de una sensación de libertad total. Contaremos con un traje espacial que nos permitirá movernos fácilmente gracias al sistema de propulsión y con una nave cuyo manejo resulta para nada complicado. Al estar en un bucle temporal, en cuanto muera nuestro personaje, este volverá al planeta de origen sin perder el progreso acumulado.

Y ahora para cerrar, te dejo un par de preguntas. ¿Qué juegos indie has jugado recientemente? ¿Qué opinas de este tipo de juegos?
Ahora sí, hasta la próxima. ¡Chau chau!🐧


recursos:

https://www.pexels.com/
