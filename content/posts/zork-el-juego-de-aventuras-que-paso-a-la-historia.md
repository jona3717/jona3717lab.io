---
title: "Zork El Juego De Aventuras Que Paso a La Historia"
date: 2023-05-23T17:45:23-03:00
draft: false
toc: false
images:
tags:
  - juego
  - videojuego
  - game
  - zork
  - retro
  - juego de texto
  - juego de rol
  - rol
---

{{< image src="/images/zork.jpg" style="border-radius: 3px;" >}}

## ¿Qué es Zork?


Zork es un juego de aventura basado en texto desarrollado por Tim Anderson, Marc Blank, Bruce Daniels y Dave Lebling en 1977. Fue lanzado para computadoras a partir de 1980 y consta de tres títulos:

- Zork I: El Gran Imperio Subterráneo
- Zork II: El Mago de Frobozz
- Zork III: El Maestro del Calabozo

 Para 1984, solo el primer episodio ya había vendido más de ciento ochenta mil copias y para 1986, entre los tres episodios se vendieron más de 680 000 copias. En su momento, este fue el mejor juego de aventuras que se había hecho. Hoy en día, es considerado uno de los mejores juegos por los críticos. Zork se convirtió en la base del género de aventuras y en un gran referente para los MUD (Multi User Dungeon) y los MMORPG (Massively Multiplayer Online Role Playing Game).
En 2007, Zork fue incluido en el canon de juegos de la Biblioteca del Congreso de Los Estados Unidos como uno de los diez juegos más importantes de la historia.

{{< image src="/images/biblioteca-congreso.jpg" style="border-radius: 3px;" >}}

 El juego consta de explorar el Gran Imperio Subterráneo en busca de un tesoro, mientras se interactúa con objetos. Todo esto por medio de comandos que son interpretados por el juego. A medida que le vamos indicando al juego lo que queremos que haga nuestro personaje, este nos responderá describiendo los lugares y los resultados de nuestras acciones.
Para completar el juego, tendremos que resolver acertijos, recolectar tesoros y atravesar cientos de lugares. Podremos interactuar con cada objeto, obstáculo o criatura que encontremos dentro de estos. Los comandos pueden ser de una o más palabras, por ejemplo: "abre la puerta" o "norte"; además podemos usar frases un poco más complejas como: "guarda la lámpara y la espada en el bolso". Obviamente, para que el comando funcione, debe haber una lámpara y una espada, de lo contrario el juego nos pedirá que volvamos a indicar lo que queremos que haga.

{{< image src="/images/zork-gameplay.jpg" style="border-radius: 3px;" >}}

Zork se lanzó en 1977 como un único título. Posteriormente, cuando se convirtió en software comercial, fue dividido en tres episodios y se agregaron nuevas secciones a los dos últimos.

No alarguemos más esto y vayamos a lo que nos compete. El juego está disponible en inglés en [Steam](https://store.steampowered.com/app/570580/Zork_Anthology/) por si lo quieres comprar. Pero calma mis niños, les dejaré un enlace al juego completo en su versión original (los tres episodios sin cortes) y en español que podrán jugar en línea y de forma completamente gratuita.

Importante aclarar, que el juego traducido es gracias a [Mauricio Díaz García](https://juegosdetexto.wordpress.com/).

[👉Zork - Juego completo en español👈](https://iplayif.com/?story=http%3A//media.textadventures.co.uk/games/GQLrNEIS8k26ddJ2nco6ag/Zork.gblorb).

Por mi parte me despido con un par de preguntas: ¿Qué opinas sobre este tipo de juegos basados en texto? ¿Has jugado a alguno?

Ahora sí, hasta la próxima. ¡Chau chau!🐧
