---
title: "Off Topic Batman Gotham by Gaslight [Opinion]"
date: 2023-12-28T12:08:30-03:00
draft: false
toc: false
images:
tags:
  - comic
  - comics
  - batman
  - off topic
  - offtopic
  - lectura
  - lecturas
---

{{< image src="/images/batman-gotham-by-gaslight-poster.jpg" style="border-radius: 3px;" >}}

Antes de comenzar, un pequeño resumen **sin spoilers**:

Es un cómic que nos presenta a la ciudad de Gotham en el siglo dieciocho. Esta historia alternativa de Batman nos muestra cómo responde la ciudad ante el encapuchado. En este cómic Batman se enfrentará a Jack el destripador.

Ahora sí, vamos con mi experiencia:

La razón principal por la que uno quiere leer este cómic, es por el villano al que tendrá que enfrentar Batman. Sin embargo, quiero empezar aclarando algo; Jack no es el arco principal ni mucho menos el punto central de la historia, la historia trata de la ciudad de Gotham, su gente y sus ideas.
Digo esto porque tal vez a más de uno le pase lo que a mí. Yo creía que la historia trataba del destripador, pero me parece que no es la mejor forma de verlo.

Este cómic se divide en dos partes: Luz de gas y Amo del futuro; ya solo empezar a leer las primeras páginas de luz de gas uno puede intuir quién es nuestro misterioso criminal y a la mitad de luz de gas (o incluso antes) estaremos seguros de saber quién es. Obviamente, si uno va con la intención de encontrarse con una trama extensa sobre como el encapuchado lucha para detener y descubrir al destripador, se encuentra con un arco que se queda pequeño y resulta predecible, sin embargo hay que ver más allá. El quid de la cuestión es el móvil de los crímenes y lo que pueden causar en los ciudadanos. Y para dejar esto mas que claro, el apartado artístico es lo que le da a Luz de gas el toque mágico que lo convierte en un clásico. Te embulle en escenas de terror y misterio y cada viñeta cobra vida.

Al llegar a la segunda parte sentí que se me había quedado corto, por lo que mencioné arriba, así que a ti que estás leyendo no te pasará, porque ya has entendido de qué va esto. ¿Verdad?

La segunda parte o el segundo tomo, es el Amo del Futuro. Nos encontramos con otro enemigo, la historia consigue atraparnos de una manera diferente y el trabajo de arte cambia al dejar atrás ese tono oscuro que sabe a tenebroso. Bruce Wayne ahora trata de descubrir la razón de seguir siendo Batman y hay un romance que se desarrolla bien y enriquece la historia.

No puedo decir que entre Amo del futuro y Luz de gas tenga una preferida, porque cada una destaca de manera diferente. Lo que puedo decir es que me la he pasado muy bien leyendo este cómic y ahora que estoy escribiendo esta breve "reseña" me han entrado ganas de volver a meterme en esa Gotham del siglo antepasado.

Espero que les haya gustado. ¡Hasta la próxima!🐧
