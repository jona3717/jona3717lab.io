---
title: "Cambiar la Distribución del Teclado en Plasma"
date: 2021-09-25T19:17:42-03:00
draft: false
tags:
  - linux
  - gnu
  - gnu linux
  - archlinux
  - debian
  - ubuntu
  - tricks
  - plasma
  - kde
---

{{< image src="/images/distribucion-teclado.png" style="border-radius: 3px;" >}}

Hay ocaciones en las que necesitamos cambiar la distribución de nuestro teclado, ya sea porque tenemos un ordenador con el teclado en inglés o porque queremos aprender a utilizar otra distribución, como es mi caso. Sea cual sea el motivo, la tarea no es para nada complicada y aquí te cuento como hacerlo.

{{< image src="/images/distribucion-teclado1.png" style="border-radius: 3px;" >}}

Como se puede ver en la imagen de arriba, abrimos las preferencias del sistema de Plasma y nos vamos al apartado *Dispositivos de entrada* en el panel de opciones de la izquierda. Al hacer *Click* en la opción, se nos desplegará un panel en el que debemos seleccionar la opción *Teclado* y ubicarnos en la pestaña *Distribuciones*.

{{< image src="/images/distribucion-teclado2.png" style="border-radius: 3px;" >}}

Como se ve en la imagen, debemos tener activada la opción *Configurar distribuciones*. En la parte inferior tenemos un botón *Añadir* al que debemos dar <clic>.

{{< image src="/images/distribucion-teclado3.png" style="border-radius: 3px;" >}}

Se nos abrirá un diálogo como el que se ve en la imagen, en el que seleccionaremos el idioma y la distribución que queremos establecer. Tenemos una opcion variante en la que podemos seleccionar entre las distintas variantes disponibles para el teclado que seleccionemos, no hará falta hacer uso de esta opción si solo quieres un teclado querty en español, que es el que se usa comúnmente.

Una vez seleccionado todas las opciones que deseamos, solo hará falta dar *Click* en *Aceptar* y luego en *Aplicar*.

No olvides compartir. 🐧
