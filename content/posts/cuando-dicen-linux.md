---
title: "Cuando Dicen Linux"
date: 2020-07-18T16:37:29-03:00
draft: false
toc: false
images:
tags:
  - linux
  - opinion
  - gnu/linux
---

{{< image src="/images/cuando-dicen-linux.jpg" style="border-radius: 3px;" >}}

Tengo ya un tiempo como usuario de sistemas operativos GNU/Linux, tiempo en el que me he topado con distintas situaciones en las que he aprendido distintas cosas y es de algunas de ellas, de las que quiero hablarles hoy.

Cuando llegué a la etapa de evangelizador, etapa a la que llegamos todos en algún momento ya sea con Linux o con cualquier otra cosa, noté que al hablar de este sistema habían dos grupos de personas, los que sí lo conocían y los que no. Grupos que a su vez se dividían en: usuarios, conocedores, los que se interesaban y los que no. Para aclarar un poco esto, les comentaré un poco de cada grupo.

**Usuarios:**
Estas personas, son las que obviamente conocen del tema, se interesan en el mismo y las hay de todo tipo, los que ayudan si tienes problemas o dificultades, los que (como yo) crean contenido para ayudar a otros a conocer mejor sobre el tema, los que no critican y un largo etcétera.

**Conocedores:**
Aquí me refiero a aquellas personas que conocen de Linux, pero que no son usuarios, algunos que lo probaron tal vez, pero solo hasta ahí quedaron. Esas personas son comúnmente, usuarios de sistemas operativos privativos.

**Los que se interesan:**
Estas personas son con las que uno más quiere encontrarse, aunque son pocas las veces que sucede realmente. Personas que no conocían de la existencia de este software y quedan intrigadas y a veces hasta maravilladas cuando se enteran de las posibilidades y las características de los SO libres.

**Los que no se interesan:**
Son personas que nada quieren saber del tema, porque poco saben de lo relacionado a este. Usualmente son personas que quieren simplemente algo en lo que poder trabajar, que conozcan o que no cambie. Sí, acá usualmente se encuentra uno con el de oficina que grita al cielo porque los de TI le actualizaron los programas, y sin importar que se vea mejor o no, ruegan porque se les regrese a la versión anterior.

Ahora que pudieron conocer un poco de cada grupo de personas, les comentaré de su reacción, cuando les dicen “Linux”.

**Conocedores:**
Mi padre por ejemplo, a quien quisiera invitar en alguna oportunidad a la web, es un usuario fiel de las ventanas que ha tocado Ubuntu, pero solo por cuestiones laborales. Y poco más que instalar el SO y alguno que otro paquete, no ha probado.

Algo muy curioso es que, al preguntarles a estas personas por qué no siguieron usando o probando el sistema operativo, la mayoría de veces, la respuesta es que es muy complicado o que es diferente. Y tienen razón, si utilizan distribuciones con entornos de escritorio como GNOME o Unity, claro que este último ya casi no se ve. Sin embargo no es solo eso, muy a parte del entorno de escritorio, los usuarios ven la manera de hacer las cosas en el SO. Buscan Word, Chrome, Paint, pero no los encuentran. Quieren instalar un programa y en los tutoriales aparecen ventanas con letras que forman palabras extrañas como: apt, sudo, wine, add-repo, etc. ¿Cómo no salir corriendo? Al ver eso y compararlo con dos clics, ponen un candado y se cierran en su idea. Claro que hay algunos que sí van por más, esos curiosos que quieren saber más o los que necesitan saber más.

**Los que se interesan:**
Para darles un mejor ejemplo de esto, les contaré algo que me pasó mientras estudiaba desarrollo. En el salón éramos poco menos de treinta alumnos y solo yo usaba Linux en mi ordenador. Recuerdo que la primera vez que abrí el portátil en el aula las personas de los lados se quedaron mirando mi monitor mientras encendía, pero hasta ahí nadie decía nada. Hasta que se vio un escritorio con dos paneles, un menú de inicio pequeño y sobrio y las ventanas que se movían como gelatina al arrastrarse sobre este. Fue entonces cuando empezaron las preguntas sobre nombres de parche y programas. A ese punto ya no le decía a todo el mundo que usara Linux, las personas preguntaban y yo solo contestaba. Tenía Arch junto con Plasma instalados y traía ya la característica para activar las ventanas tambaleantes. Les comenté un poco de esto y de aquello y eso fue todo. Con el asar del tiempo comenzaron a preguntar si se puede hacer esto y aquello en Linux, si es igual o no, si hay muchas diferencias, si es muy complicado o si es mejor. A mediados de ciclo académico (seis meses) ya la mitad del salón tenía instalado Linux en sus ordenadores, algunos aún preguntaban y otros criticaban cuando no encontraban alguna cosa; para el final del ciclo solo dos de mis compañeros tenían Linux en sus ordenadores. Les gustaba cambiar la apariencia cada semana y no tener que instalar cracks para tener sus programas. Sí, eran dos de casi treinta, pero eran dos que estaban contentos con el SO que tenían instalado en su ordenador, incluso uno de ellos es gamer.

Linux es para todos, no es complicado y si solo eres un usuario final que no quiere tener que ver con la terminal, no tiene por qué hacerlo. Acá también puede trabajar con solo dos clics, es increíble lo mucho que se ha desarrollado este sistema operativo.

**Los que no se interesan:**
De la mitad restante en el salón, habían dos personas que pidieron que se les instale el SO en sus ordenadores, pero no querían probarlo, no lo querían diferente, querían el botón de las ventanas y los programas que conocían, no querían el cambio visual. Era algo como: “Cambia el SO pero no lo quiero ver diferente”. Sin embargo no a todos quieren ver el sistema como lo conocen.

Mi hermana es una persona de este grupo, pero a ella no le molesta un “pequeño” cambio. Le puse KDE Neon en el portátil porque era el único que tenía a mano y una vez instalado, solo le mostré el entorno de escritorio. El botón inicio ya no estaba más, pero había un botón que mostraba el menú de aplicaciones, no había la suite de ofimática que conocía, pero tenía una que le permitía trabajar y con la que le valía para lo que necesitaba, no tenía que descargar aplicaciones de internet sino que entraba en una tienda como en el móvil; a ella le valía. No le interesaba saber qué era, si era libre o no, solo le interesaba que funcionara para ella. Ya tiene un año con Linux en su ordenador, pero ella no sabe y tampoco quiere saber. Mientras le sirva, va a seguir trabajando con él.

No olvides compartir.
