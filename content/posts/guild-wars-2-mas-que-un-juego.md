---
title: "Guild Wars 2 más que un juego"
date: 2023-03-14T11:53:06-03:00
draft: false
toc: false
images:
tags:
  - games
  - videojuegos
  - opinion
  - reseña
  - juegos
  - guild wars 2
  - GW2
---

{{< image src="/images/guild-wars-2.jpg" style="border-radius: 3px;" >}}

Guild Wars 2 es un juego MMORPG desarrollado por [ArenaNet](https://arena.net/), es decir, es un juego de rol, multijugador masivo, en linea. Para poner un ejemplo del género, es como World of Warcraft.

El juego se desarrolla en Tyria, un mundo habitado por humanos, asura, enanos, norn y otras criaturas. Hay tres principales continentes conocidos: Tyria, Cantha y Elona, en los que se desarrolla la historia del juego; además de estos continentes hay muchísimos lugares sin explorar.  Tyria es un mundo cambiante, con paisajes hermosos, criaturas increíbles y mucho más.

Hay muchas características que hacen a Guild Wars 2 un juego increíble, por lo que no podría mencionar todas pero les hablaré de algunas:

## Historias personalizadas:
Cada jugador tiene una experiencia de juego distinta, ya que esta depende de las decisiones que tomes y la personalización de tu personaje. En este juego la historia no empieza cuando tu personaje aparece en el mundo y lo empiezas a recorrer, aquí la historia empieza con la creación de tu personaje, ya que deberás seleccionar la raza, que determina en dónde tendrá su hogar y en dónde empezarás a jugar, deberás responder varias preguntas mientras creas tu personaje y cada respuesta hará la diferencia en la historia del mismo. La historia personalizada hará que viajes por capítulos que te irán mostrando las aventuras de tu personaje a lo largo de su vida mientras forman su propio destino. Puedes jugar con amigos para que te ayuden en determinadas ocasiones, pero eso no cambiará el curso de tu historia, ya que las decisiones las seguirás tomando tú, ellos serán espectadores de las decisiones que tomes y del curso de tu historia.

{{< image src="/images/gw2-2.jpg" style="border-radius: 3px;" >}}

## Eventos dinámicos:
El mundo abierto de Guild Wars 2 está plagado de peligros, pero no quiere decir que estás obligado a formar un grupo para sobrevivir en él. Este mundo está diseñado para incentivar el juego en equipo, pero no es necesario que formes un grupo, puedes unirte al combate con otros jugadores que ya se encuentran ahí. No existe ninguna penalización por unirte al combate o por recibir ayuda. La experiencia y recompensas son individuales por lo que todos recibirán su parte y aunque lo hagas solo, el botín no cambiará.
Aquí tus elecciones afectan al mundo que te rodea. Cada zona cuenta con eventos dinámicos que hacen de Tyria un mundo con vida. Podrás ver la historia sucediendo ante tus ojos. Estos eventos están sujetos a la presencia de los jugadores, por lo que si hay más jugadores mayor será la dificultad y si el resultado es una victoria o una derrota, el entorno cambiará de acuerdo a ello.
Los habitantes de Tyria están vivos y si te paras a descansar o simplemente a apreciar el lugar, podrás escuchar conversaciones entre ellos o verlos en peligro y tú decidirás si vas en su ayuda o no. ¿Faltaba algo? Por supuesto, si sigues a algún personaje que te resulte sospechoso, puede que encuentres su tesoro escondido o la trampa mortal que ha estado preparando.

{{< image src="/images/gw2-3.jpg" style="border-radius: 3px;" >}}

## Combate:
Puedes usar armas de fuego, espadas, dagas, lanzas o inclusive armas de asedio, entre muchas otras para el combate. Ya sea en PvE o PvP. Hay que mencionar que en PvP hay un modo Mundo contra mundo, en el que cientos de jugadores que forman tres ejércitos, se enfrentan entre ellos y luchan por el control de castillos y fortalezas durante partidas semanales y torneos de temporada, una guerra puede durar dos semanas, sí dos semanas.
No importa que seas un novato, si entras al modo PvP, todos los personajes están igualados, todos los personajes se ajustan al nivel máximo y todos tienen acceso a las mismas armas y armaduras de alto nivel.

{{< image src="/images/gw2-4.png" style="border-radius: 3px;" >}}

Guild Wars 2 es enorme, hermoso y atractivo. Me encantaría poder hablar mucho más de este juego, pero tendría que hacer todo un documental para ello y aún así me dejaría cosas fuera. Recorrer Tyria es una de las cosas más asombrosas, descubrir nuevos lugares, con paisajes y vistas asombrosas… No tiene comparación. Pero no me creas solo porque lo digo, pruébalo, recorre los tres continentes, habla con los ciudadanos, bebe licor en sus bares, descubre esta maravilla de juego.

{{< image src="/images/gw2-5.png" style="border-radius: 3px;" >}}

Momentito, que me olvidaba de algo importante. Al principio, puse de ejemplo al WoW, ya que ese también es un juego del mismo estilo, pero atento aquí. En Guild Wars 2 no necesitas de una suscripción para poder jugar, además el juego base es gratis por lo que puedes descargarlo, probarlo y si te gusta puedes comprar las expansiones disponibles, con las que obtendrás también la oportunidad de pescar, planear o recorrer el mundo en una montura. El juego está disponible en [Steam](https://store.steampowered.com/app/1284210/Guild_Wars_2/) o puedes simplemente descargarlo desde la web de [ArenaNet](https://account.arena.net/welcome).

Ahora sí, chau chau. 🐧
