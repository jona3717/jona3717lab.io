---
title: "Usar Control Xbox Con Dongle en GNU/Linux"
date: 2023-11-04T10:13:41-03:00
draft: false
toc: false
images:
tags:
  - gnu
  - linux
  - gnu/linux
  - archlinux
  - gaming
  - xbox
  - xbox one
  - xbox series
  - control xbox
  - xbox controller
  - mando xbox
  - controller
  - control
  - mando
---

{{< image src="/images/xbox-dongle-controller.jpg" style="border-radius: 3px;" >}}

### Introducción

Hace casi un año publiqué una entrada en el blog sobre cómo instalar el controlador para poder usar nuestro control de Xbox en GNU/Linux. Hoy agregamos esta entrada a modo de complemento, para quienes prefieran utilizar el dongle al conectar su control. En mi caso, ha sido algo medio forzoso, ya que al usar mi control con Bluetooth me daba muchos problemas y la última vez que probé se desconectaba a los dos minutos. Por ello busqué una forma de jugar sin depender del Bluetooth y esta fue la solución.

### Compatibilidad

- Conexión por cable (vía USB).
- Conexión inalámbrica (con el dongle de Xbox).

No cuenta con conexión Bluetooth, por lo que si quieres conectar el control por bluetooth, lo mejor será que utilices [xpadneo](http://localhost:1313/posts/usar-control-xbox-en-gnu-linux/).

### ¿Qué es el Dongle?

Es un adaptador inalámbrico de Xbox para Windows 10/11 que permite conectar hasta 8 controles a la vez. Admite cualquier control inalámbrico de Xbox.

### Preparación

Necesitamos el driver o controlador para que Linux reconozca el dongle, para esto usaremos xone. Es un controlador para accesorios de Xbox One y Series X|S. Este controlador viene a ser un reemplazo actualizado para xpad, utilizando el Game Input Protocol de Microsoft (GIP).

El controlador Xone lo encontramos en Github:

- https://github.com/medusalix/xone

Algunos paquetes:

- DKMS
- curl (para descargar el firmware)
- cabextract (para extraer el firmware)

### Instalación

Los pasos de instalación se pueden encontrar en la descripción del repositorio, pero por si no quieren leerlos ahí, acá los tienen. Recuerden desconectar cualquier dispositivo Xbox que tengan conectado si fuera el caso.

1. Clonar el repositorio:
```
git clone https://github.com/medusalix/xone
```
2. Instalar:
```
cd xone
sudo ./install.sh --release
```
3. Descargar el firmware para el dongle inalámbrico:
```
sudo xone-get-firmware.sh
```


### Actualización

Siempre es bueno mantener nuestro software actualizado. Para actualizar Xone, simplemente desinstalamos y volvemos a instalar, a continuación les dejo los pasos para desinstalar.

1. Desinstalamos (Para esto debemos estar dentro de la carpeta del repositorio que clonamos).
```
sudo ./uninstall.sh
```

### Nota importante:
En la descripción del repositorio, indica que para conectar nuestro dispositivo es necesario que este haya sido emparejado con el dongle previamente, ya que no se conectarán al dongle si antes fueron conectados con USB o Bluetooth. Sin embargo, yo tuve conectado mi control con Bluetooth y luego lo conecté directamente al dongle sin problema presionando el botón del dongle una vez para que entrara en modo emparejamiento y haciendo lo mismo en mi control. Solo es cuestión de probar.

Ahora solo queda conectar el control y disfrutar. ¡No olvides compartir! 🐧
