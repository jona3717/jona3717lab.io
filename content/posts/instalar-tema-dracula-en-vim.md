---
title: "Instalar Tema Dracula en Vim"
date: 2020-07-18T20:18:33-03:00
draft: false
toc: false
images:
tags:
  - tutorial
  - vim
  - linux
  - gnu/linux
  - editor de codigo
  - code editor
  - editor vim
  - editor
  - dracula
  - tema vim
  - instalar tema vim
---

{{< image src="/images/dracula-vim.png" style="border-radius: 3px;" >}}

**Introducción**

Vim es un potente editor cuenta con muchos plugins para poder adaptarlo a nuestras necesidades, pero no solo eso sino que también podemos hacer que se vea mucho más atractivo. Por ello, hoy les dejo este post a modo de tutorial con el que podrán aprender a instalar el tema dracula o tener lo necesario para instalar cualquier otro tema

**¿Qué se necesita?**

1. Instala git.
2. Descarga el tema dracula con git.
```
git clone https://github.com/dracula/vim/
```
3. Entra en la carpeta dracula y copia los archivos necesarios.
```
cd dracula
cp -r color ~/.vim/
cp -r autoload/* ~/.vim/autoload/
```
4. Ahora editamos el archivo de configuración de vim para activar el tema.
```
vim ~/.vimrc
```
Verificamos que las siguientes líneas estén en el archivo, de no ser así, las agregamos.
```
syntax on
colorscheme dracula
```
5. Salimos de vim y volvemos a abrirlo y voilá, el tema ha sido instalado.

Espero que este pequeño tutorial te haya servido.

No olvides compartir.
