---
title: "Acelerar La Instalación De Paquetes en Archlinux O Derivadas"
date: 2022-11-01T14:09:26-03:00
draft: false
toc: false
images:
tags:
  - archlinux
  - hakcs
  - tip
  - pacman
  - gnu
  - linux
  - gnulinux
---

{{< image src="/images/pacman.jpg" style="border-radius: 3px;" >}}
Image by <a href="https://pixabay.com/users/koikeyusuke-15679770/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=7024232">YUSUKE KOIKE</a> from <a href="https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=7024232">Pixabay</a>

### Introducción

Instalar, desinstalar y actualizar paquetes en Archlinux es una de las cosas de rutina que disfruto hacer. No por la acción, sino por usar **pacman**; el gestor de paquetes de esta distribución se ha vuelto mi gestor de paquetes por excelencia, es increíble lo rápido que hace las cosas y es absurdamente sencillo lanzar los comandos. En fin no estamos acá para vanagloriar a pacman, sino para acelerarlo aún más cuando realiza una tarea.

### ¿Cómo funciona?

Pacman utiliza uno de los núcleos del procesador para compilar los paquetes. Lo que haremos será aumentar la cantidad de núcleos que usará para compilar. Para esto tenemos que saber cuántos núcleos tiene nuestro procesador y editar el archivo de configuración de MakePKG.

### Los núcleos del procesador

Para saber cuántos núcleos tiene nuestro procesador, podemos usar el siguiente comando:

```bash
lscpu | grep '^CPU(s):'
```

### Editando el archivo de configuración

El archivo lo encontramos en la carpeta /etc, y para editarlo podemos usar el editor de texto de nuestra preferencia, de más está decir que debemos abrir el archivo con permisos de superusuario, ya que de otro modo no se guardarán los cambios. En mi caso usaré neovim para editar el archivo, por lo tanto el comando sería:

```bash
sudo nvim /etc/makepkg.conf
```

Una vez abierto el archivo debemos buscar la siguiente línea: `MAKEFLAGS="-j2"`. Puede que la tengas comentada con un `#` al principio de la línea, de ser así debes borrar el símbolo para descomentar la línea.

Para indicar la cantidad de núcleos que queremos que use, simplemente cambiaremos el número 2 por el la cantidad de núcleos más uno, es decir:

Si queremos que use 4 núcleos la línea quedará: `MAKEFLAGS="-j5"`.

Una vez hecho el cambio, solo debemos guardar el archivo y habremos terminado con el proceso. Si bien es posible hacer que pacman use todos los núcleos, no es algo que yo haría, en mi caso le pongo como mucho la mitad del total de núcleos.

Espero que te haya servido. No olvides compartir. 🐧
