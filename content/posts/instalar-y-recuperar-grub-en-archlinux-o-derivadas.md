---
title: "Instalar y Recuperar Grub en Archlinux O Derivadas"
date: 2022-08-02T11:06:49-03:00
draft: false
tags:
  - linux
  - gnu
  - gnulinux
  - arch
  - archlinux
  - grub
  - grub2
  - tutorial
  - instalar
---

{{< image src="/images/grub.png" style="border-radius: 3px;" >}}

## Introducción

Grub es un gestor de arranque que permite al usuario, seleccionar el sistema operativo o la distribucíon al encender el equipo. Es ese popular menú que nos da las opciones en el dual boot o con el que seleccionamos el kernel con el que queremos arrancar nuestra distro.

Por lo menos una vez en toda nuestra vida linuxera nos ha pasado o nos está por pasar que perderemos el grub. Ya sea por trastear, tratar de cambiar el tema, o travesear con los archivos. Por lo que saber reparar nuestro gestor de arranque es algo que termina por quedarse grabado en nuestro cerebro como el manejar bicicleta, "Una vez que aprendes, nunca se te olvida". Así que hoy les dare los pasos para instalar y recuperar el grub en Archlinux o cualquier derivada. También sirve para otras distribuciones, pero hay que tener en cuenta que los comandos de instalación variarán dependiendo de en qué distro te encuentres.

## Recuperar Grub

Para recuperar el grub, necesitamos una memoria USB con nuestra distribución cargada, es decir un live-USB. Arrancamos nuestro live-USB y dependiendo de si estamos en Archlinux o no, tendremos que abrir una terminal.

**Montamos las particiones:** Esto va ha depender de cuantas particiones creamos al instalar nuestro sistema. Si el particionado se hizo de forma automática tendremos que ver cuántas y cuáles son:

```
fdisk -l
```
Esto nos mostrará todos los dispositivos de almacenamiento y sus particiones, hay que identificar cuales son las particiones de nuestro disco y cuales son las de nuestro live-USB.

En caso de tener una sola partición solo haremos lo siguiente: (las **X** en los nombres de los discos varian dependiendo la letra del dispositivo y el número de partición respectivamente).

```
mount /dev/sdXX /mnt
```
Si tenemos la partición de `/boot` separada, deberemos montarla a continuación:

```
mount /dev/sdXX /mnt/boot
```

Ahora que tenemos montadas las particiones accedemos al entorno **chroot**.

```
arch-chroot /mnt
```
Una vez dentro, nos aseguramos de tener instalado el paquete Grub y continuamos con la instalación. 

## Instalación

Aquí la cosa es muy sencilla, debemos saber si el ordenador tiene la BIOS en modo UEFI o Legacy, y con eso en mente podemos continuar.

```
sudo pacman -S grub
```
**UEFI:**

```
sudo grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
```
**Legacy:** Hay que tener en cuenta que `/dev/sdX` es el disco en el que instalaremos el grub y no el número de la partición.

```
sudo grub-install --target=i386-pc /dev/sdX
```

Una vez hecho eso solo queda generar el archivo de configuración:

```
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

Ahora lo único que hace falta es reiniciar el ordenador. Si recuperaste el grub, te queda un paso más:

Salimos del entorno chroot:

```
exit
```
Desmontamos las particiones:

```
umount -R /mnt
```

Ahora sí, reiniciamos:

```
reboot
```

Espero que este tutorial haya sido de ayuda. Y recuerda, si te ayudó:

No olvides compartir. 🐧
