---
title: "Revelaciones de KDE Plasma 6"
date: 2023-05-13T23:28:14-03:00
draft: false
toc: false
images:
tags:
  - gnulinux
  - gnu
  - linux
  - kde
  - kdeplasma
  - kde plasma
  - plasma
  - plasma6
  - noticias
  - noticia
---

{{< image src="/images/plasma-portada.png" style="border-radius: 3px;" >}}

## Introducción

Hace mucho que esperamos la llegada de Plasma 6. En lo personal, a penas se liberó la versión 6 de Qt ya me estaba preguntando cuánto podrían tardar en liberar la versión 6 de Plasma; por ahora aún no tenemos una fecha exacta, pero lo que traigo hoy es una dosis que calmará las ansias, al menos a mí me ha servido.

Hace poco hubo un evento en Alemania llamado Plasma Sprint 2023,luego del evento, el miembro de la junta directiva de KDE eV, Nate Graham, dio a conocer algunas de las características de KDE Plasma 6 en [su blog](https://pointieststick.com/2023/05/11/plasma-6-better-defaults/?ref=news.itsfoss.com). Repasemos algunas de ellas:

## Doble clic por defecto

{{< image src="/images/plasma-doble-click.png" style="border-radius: 3px;" >}}

Esto es algo que en algunas distribuciones ya se encuentra activo, sin embargo, hasta ahora la opción por defecto ha sido que con un solo clic se abrieran archivos o carpetas. De ahora en adelante, la opción predeterminada será la de selección. Cuando se haga un clic sobre algún archivo o carpeta, este será seleccionado y para abrirlo se deberá hacer doble clic. Para muchos puede ser cosa de nada, pero seguro que a más de uno ha tenido que ir a los ajustes para cambiar esto luego de instalar el entorno de escritorio en su distribución.

## Wayland por defecto
En Plasma 6, la sesión por defecto será Wayland; sin embargo, la sesión X11 se mantendrá como una opción adicional para el que desee. En lo personal, es uno de los cambios que con más ilusión/miedo espero, ya que hasta ahora no he tenido suerte con mi sesión en Wayland.
Para esta nueva versión el desarrollo se está enfocando en solucionar muchos problemas entre ellos varios de NVIDIA. Esperemos que todo vaya viento en popa.

## Panel flotante por defecto

{{< image src="/images/plasma-panel-tareas.webp" style="border-radius: 3px;" >}}

Actualmente podemos activar el modo flotante del panel de tareas, en Plasma 6, esta será la opción por defecto. Una opción menos que tendré que configurar.

## Área de encabezado acentuada con colores para distinguir las ventanas activas

{{< image src="/images/plasma-ventanas-color.png" style="border-radius: 3px;" >}}

Esto sirve para ayudar visualmente al usuario a identificar mejor una ventana activa. El encabezado de esta estará acentuada con el conjunto de colores de énfasis que se haya establecido.

## Nuevo selector de tareas

{{< image src="/images/plasma-selector-tareas.webp" style="border-radius: 3px;" >}}

El selector de tareas es una de las cosas a las que también les dedico tiempo cuando estoy personalizando mi distribución en una nueva instalación. En Plasma 6 se a agregado un selector de "Cuadrícula de miniaturas", en el que se puede ver una miniatura y el icono de la aplicación.

## No más "scrolling" en el escritorio para navegar entre los escritorios virtuales por defecto.
¿A quién no le ha pasado que por mover la rueda del ratón, muchas veces sin querer se le ha movido el escritorio cual ruleta de feria? Actualmente ya no utilizo escritorios virtuales, sin embargo, cuando los usaba no hace mucho, me sucedía con relativa frecuencia. En Plasma 6 han escuchado a la comunidad y esta función quedará desactivada de forma predeterminada. De todos modos, siempre podremos volver a activarla.

Hay algunos cambios más que puedes ver en el blog de Nate Graham. Aquí puedes verlos[Aquí puedes verlos](https://pointieststick.com/2023/05/11/plasma-6-better-defaults/?ref=news.itsfoss.com).

No olvides compartir. Chau, chau. 🐧
