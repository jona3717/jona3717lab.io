---
title: "Configurar Vim Para Trabajar Con C++ es posible con con C.vim y OmniCppComplete"
date: 2020-07-18T21:08:38-03:00
draft: false
toc: false
images:
tags:
  - configuracion
  - vim
  - nvim
  - c++
  - cpp
  - plugins
  - plugin vim
  - cvim
  - omnicppcomplete
---

{{< image src="/images/vim-cpp.png" style="border-radius: 3px;" >}}

## Introducción

Llevo un tiempo usando Vim y me ha resultado muy productivo, sin embargo es posible aumentar la productividad haciendo uso de plugins. Es por ello que esta vez quiero comentarles sobre OmniCppComplete, un plugin para vim que proporciona autocompletado para el desarrollo de C y C++, y c.vim, un plugin que facilita la programación en C y C++ con caraterísticas como comentarios de cabecera y funciones, automatización con algunos fragmentos de código y otras más. Esto es realmente útil, cuando se está programando y puedo decir, pese a mi corta experiencia, que es una muy buena opción para realizar esta tarea.
## Instalación
## Requisitos:

1. Descarga el plugin desde [aquí.](https://www.vim.org/scripts/download_script.php?src_id=21803)
2. Una terminal.

## Instalación CVim:

1. Una vez descargado, copiamos el archivo a la ruta /usr/src/.
```
sudo cp cvim.zip /usr/src/
```
2. Descomprimimos el archivo desde la terminal en la ruta /usr/src/.
```
sudo unzip /usr/src/cvim.zip
```
3. Activamos el plugin agregando la siguiente línea en el archivo ~/.vimrc.
```
filetype plugin on
```
## Configurar OmniCppComplete:

https://www.thegeekstuff.com/2009/01/tutorial-make-vim-as-your-cc-ide-using-cvim-plugin/https://www.thegeekstuff.com/2009/01/tutorial-make-vim-as-your-cc-ide-using-cvim-plugin/1. Agrega las siguientes líneas en el archivo ~/.vimrc
```
set tags+=~/.vim/tags/stl
noremap :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<cr>
inoremap :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<cr>
let OmniCpp_NamespaceSearch = 1
let OmniCpp_GlobalScopeSearch = 1
let OmniCpp_ShowAccess = 1
let OmniCpp_MayCompleteDot = 1
let OmniCpp_MayCompleteArrow = 1
let OmniCpp_MayCompleteScope = 1
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview
```
2. Si no tienes la carpeta ~/.vim/tags/ entonces créala.
```
mkdir -p ~/.vim/tags
```
3. Descargamos las cabeceras necesarias desde aquí.
4. Descomprimimos el archivo y sin acceder a la carpeta generamos las tags para la STL.
```
ctags -R --c++-kinds=+p --fields=+iaS --extras=+q --language-force=C++ cpp_src
```
5. Renombramos el archivo tags a stl y lo movemos a la carpeta que creamos anteriormente.
```
mv tags ~/.vim/tags/stl
```
## Uso de CVim:

Algunas de las más interesantes características de cvim son:

1. Agregar una cabecera de comenterios automaticamente, cada vez que crees un nuevo archivo .c o .cpp, puedes editar la información editando el archivo ~/.vim/c-support/templates/Templates

2. Agrega una función usando el comando: `\if`

3. Agrega la función main usando el comando: `\im`

4. Agrega un comentario de cabecera para una función usando el comando: `\cfu`

5. Agrega un cuadro de comentario usando el comando: `\cfr`

6. Incluye cabeceras o headers usando el comando: `\p<`

7. Guarda y compila el archivo usando el comando: `\rc`

8. Ejecuta el archivo usando el comando: `\rr`

9. Inserta fragmentos de código predefinidos usando el comando: `\nr`

**Nota:** Puedes crear tus propios fragmentos de código para tenerlos a la mano seleccionando el código que quieres guardar y pulsando el comando: \nw

Puedes encontrar más inormación en [esta web.](https://www.thegeekstuff.com/2009/01/tutorial-make-vim-as-your-cc-ide-using-cvim-plugin/)

No olvides compartir.
