---
title: "Archlinux O Cualquier Distro en Tu Movil"
date: 2020-09-24T15:03:22
draft: false
tags:
  - linux
  - android
  - gnu
  - gnu linux
  - termux
  - anlinux
  - archlinux
  - debian
  - ubuntu
  - hacking
  - tricks
  - fdroid
  - terminal
---

{{< image src="/images/archlinux-termux.jpg" style="border-radius: 3px;" >}}

## Introducción

Una de las cosas que siempre he buscado, es tener el sistema operativo que quiera en el dispositivo que tenga, sin importar cual sea. Todo esto, con el fin de tener la flexibilidad de realizar cualquier tarea que me proponga en cualquier momento, sin tener que depender de un ordenador o un dispositivo móvil. Si bien es cierto, que tener un sistema operativo de escritorio con interfaz de usuario en el móvil puede ser un poco incómodo por no contar con una pantalla lo suficientemente grande, eso no quiere decir que la tarea sea imposible. Para ejemplo, este artículo se está escribiendo en vim desde un teléfono móvil que corre Archlinux. Ahora bien, como les comenté líneas atrás, un SO con interfaz gráfica, puede resultar incómodo, por lo que este artículo tratará sobre la instalación o mejor dicho, configuración de una distribución GNU Linux en un dispositivo con Android.

## Requisitos

- Dispositivo con Android instalado (no es necesario root)
- FDroid
- AnLinux
- Conexión a internet

### Fdroid

Es una "tienda de aplicaciones" de código abierto y licencia libre.

Puedes descargar el instalador [aquí.](https://f_droid.org/F_Droid.apk)

### Termux

{{< image src="/images/fdroid-termux.jpg" style="border-radius: 3px;" >}}

Un emulador de terminal para android, puedes encontrarlo en la PlayStore o en FDroid.

### AnLinux

{{< image src="/images/fdroid-anlinux.jpg" style="border-radius: 3px;" >}}

Esta aplicación es la que hace la magia. Te permite seleccionar la distribución que prefieras, de una lista preestablecida, para luego poder configurarla en tu terminal. Entre las opciones podemos encontrar: Archlinux, openSUSE, Ubuntu, Kali, etc.

Puedes descargar la aplicación desde FDroid.

## Proceso

Una vez que descargamos todo lo necesario, abrimos AnLinux. ahí, se nos mostrará la siguiente pantalla:

{{< image src="/images/anlinux.jpg" style="border-radius: 3px;" >}}

Seleccionamos la distro que queramos instalar presionando el botón **choose**. Se nos mostrará una lista con las distribuciones disponibles.

{{< image src="/images/selecciona-distro.jpg" style="border-radius: 3px;" >}}

En esta ocación seleccionamos Archlinux, nos aparecerá un aviso que nos indica que es más grande que otras y que requiere un poco más de trabajo instalar.

{{< image src="/images/aviso.jpg" style="border-radius: 3px;" >}}

Ahora veremos que nos aparecen unos comandos, estos nos van a servir para la instalación que realizaremos a continuación; para ello debemos pulsar en el botón copy.

{{< image src="/images/ver-comandos.jpg" style="border-radius: 3px;" >}}

Ahora que tenemos los comandos en el portapapeles, presionamos el botón launch, que nos abrirá Termux.

{{< image src="/images/aviso.jpg" style="border-radius: 3px;" >}}

Pegamos ahí el comando, haciendo una pulsación prolongada y presionamos enter. Comenzará a descargar los archivos necesarios y a configurar nuestra distribución. Una vez haya acabado, deberemos ejecutar unos cuantos comandos más para tener la distro funcionando.

{{< image src="/images/pegar-comando.jpg" style="border-radius: 3px;" >}}

Ejecutamos el siguiente comando para iniciar la distribución:

```
./start-arch.sh
```

Ahora ya estamos dentro de Arclinux, tenemos que ejecutar un script que nos va a permitir conectarnos a internet, para así poder actualizar l distro o instalar paquetes y demás. 
Damos permisos al script:

```
chmod 755 additional.sh
```

Ejecutamos el script:

```
./additional
```

Ahora ya no queda más que disfrutar de nuestra distribución recién instalada. Podemos actualizar la distro, crear un usuario, instalar lo que queramos y configurarla como nos plazca.

Aquí una captura de como la dejé.

{{< image src="/images/archlinux-personal.jpg" style="border-radius: 3px;" >}}
