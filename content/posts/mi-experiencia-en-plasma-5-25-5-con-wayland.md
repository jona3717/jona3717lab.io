---
title: "Mi Experiencia en Plasma 5.25.5 Con Wayland"
date: 2022-09-23T22:55:25-03:00
draft: false
toc: false
images:
tags:
  - wayland
  - plasma
  - gnu linux
  - gnu
  - linux
  - opinión
---

{{< image src="/images/plasma-wayland-5-25-5.jpg" style="border-radius: 3px;" >}}
Hace poco actualicé mi distribución, y lo más destacado fue la llegada de Plasma 5.25.5, que traía correcciones de errores para el escritorio con Wayland. Cada  vez que hay una nueva actualización, me da por probar el escritorio simplemente para ver como va el desarrollo y en qué aspectos ha mejorado Wayland, y esta vez no ha sido la excepción. Lo primero que hice fue probar un par de aplicaciones y algunos juegos. ¿El resultado? Sigo con X11, pero deja que te cuente por qué, al fin y al cabo, tal vez no usemos las mismas aplicaciones de la misma forma.

## Probando aplicaciones

### OBS Studio

Hace mucho que tenía problemas con Wayland y esta aplicación, por lo que fue la primera que abrí. El problema que vengo arrastrando, es el no poder capturar una sola pantalla. Tengo dos monitores (un Samsung como principal y un LG como extención) y cuando selecciono el monitor del que quiero grabar, solo se ve el puntero del ratón; esto no pasa si selecciono la opción de grabar toda el área de trabajo, es decir, las dos pantallas juntas. La “solución” para esto es usar esta última opción y recortar el área de trabajo para que en la imagen se vea únicamente el monitor que quiero, sin embargo no es lo mejor en cuanto a *practicidad.* 

Veamos ahora que otras cosas me pasaron.
Al iniciar la aplicación, el escritorio entra en un estado de “aletargamiento”, el puntero del ratón tiene este efecto *ghosting* en el que se ve el recorrido, y los efectos de las ventanas (en mi caso tengo dos, lámpara mágica al minimizar y ventanas tambaleantes al arrastrarlas de un lado a otro) no se ven fluidos.

Al momento de grabar, la cosa empeora en cuanto al estado del escritorio, se nota mas la falta de fluidez y la grabación tiene cortes (se traba) por momentos.

Esto puede que sea no solo por OBS, sino también porque tengo una GPU NVidia y entre Wayland y estas gráficas no termina de haber buena relación. 

### Steam y Juegos

Steam no me da problemas al abrir, todo funciona bien y no hay sorpresas desagradables. Pruebo algunos juegos y todo parece funcionar correctamente.

Luego pruebo un juego porteado y es cuando comienzan los problemas. Sorprendentemente, no son problemas gráficos ni de rendimiento, sino que son problemas con los controles. Los controles no obedecen o no como deberían, algunos botones se cambian y otros simplemente no hacen nada, las palancas no sirven y la única forma de controlar el juego es por medio del teclado.

### Otras aplicaciones

En cuanto a aplicaciones de uso diario como editores de código, la terminal, el navegador, clientes de redes sociales y otras de diseño y pintura, no encontré problemas, no probé Kdenlive pero no espero que haya problema alguno (cuéntenme si lo tuvieron). Sin embargo, al usar un cliente de Notion, se notó de nuevo una especie de aletargamiento. Al escribir, los caracteres demoran en aparecer, en resumidas cuentas, la entrada por teclado se ve afectada con un retraso importante. Puedes borrar una letra y la verás en el texto por un par de segundos antes de que por fin se borre y causa el problema de presionar nuevamente la tecla y borrar o escribir más de lo que se quería.

## Conclusión

El avance de Wayland se nota, por lo menos desde hace un par de actualizaciones ya me deja ver el escritorio y el puntero del ratón no se ve enorme. Los juegos no se cierran de la nada y las aplicaciones de uso diario no dan problemas (la mayoría), pero aún hay cosas como el “aletargamiento” con OBS, que me tiran para atrás ya que es algo que, si bien no uso a diario, si uso con frecuencia. Seguiré esperando por nuevas actualizaciones y espero seguir encontrando buenos resultados como ha sido hasta ahora.

