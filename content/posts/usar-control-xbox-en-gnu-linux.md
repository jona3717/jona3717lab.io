---
title: "Usar un control de Xbox One o Series X/S en GNU/Linux"
date: 2022-12-03T20:05:19-03:00
draft: false
toc: false
images:
tags:
  - gnu
  - linux
  - gnu/linux
  - archlinux
  - gaming
  - xbox
  - xbox one
  - xbox series
  - control xbox
  - xbox controller
  - mando xbox
  - controller
  - control
  - mando
---
{{< image src="/images/xpadneo.jpg" style="border-radius: 3px;" >}}

### Introducción

Hace ya un tiempo que estoy usando un control de Xbox para jugar en Archlinux, y durante este periodo he aprendido algunas cosas que tal vez sean de ayuda para más de uno. Ya que al ser un producto de Microsoft, puede que no resulte tan fácil como conectar y disfrutar, así que sin más, les dejo con mi pequeña contribución cual receta de cocina.

### ¿Qué necesitamos?

- Control de Xbox (One, Series S/X)
- Ordenador con GNU/Linux
- Controlador o driver

### Preparación del sistema

Para que nuestro control funcione correctamente, debemos instalar primero el controlador. Para los que estén en Archlinux, como yo, encontrarán más de uno en el AUR, pero nosotros usaremos el siguiente: `xpadneo-dkms`. Este controlador es el que me ha dado mejor resultado.

Primero verifiquemos que tenemos instalados las dependencias:

- `dkms`
- `linux-headers`
- `bluez`
- `bluez-utils`

El primer paquete es el encargado de cargar el controlador en el kernel, el segundo son las cabeceras del kernel y os dos últimos paquetes son para el bluetooth. Es muy probable que ya tengas instalados estos paquetes, pero mas vale prevenir…

### Instalando el controlador

Ahora con todo listo, haremos lo siguiente dependiendo de la distribución en la que nos encontremos.

- **Archlinux y derivadas:**
    - Instalamos el módulo desde el AUR: `yay -S xpadneo-dkms`

    Si no quieres usar el paquete del AUR, puedes seguir los pasos indicados abajo para instalar manualmente el controlador. Es importante tener en cuenta que deberás actualizar manualmente el controlador (instrucciones más abajo).

- **Otras distribuciones:**
    - Descargamos el paquete clonando el repositorio: `git clone https://github.com/atar-axis/xpadneo.git`
    - Entramos en la carpeta: `cd xpadneo`
    - Ejecutamos el script de instalación: `sudo ./install.sh`

Una vez hecho todo eso, ya podemos encender nuestro control y conectarlo por Bluetooth para disfrutar de un buen juego.

### Actualizar el controlador

Esta parte es solo para quienes instalaron el módulo de forma manual, por lo que si lo instalaste desde el AUR, solo bastará con ejecutar el comando de siempre.

- `git pull`
- `sudo ./update.sh`

### Desinstalar el controlador

Si ya no usarás el control de Xbox o prefieres probar otro controlador, siempre puedes desinstalar este. Veamos como hacerlo:

- **Archlinux:**
    - `yay -Rns xpadneo-dkms`
- **Otras distribuciones:**
    - Entramos en la carpeta: `cd xpadneo`
    - Ejecutamos el script: `sudo ./uninstall.sh`

### Conclusión y opinión

Con esto ya tienes todo listo para disfrutar de tu control de Xbox en tu distro GNU/Linux. Este controlador tiene sus ventajas: Conectar varios controles a la vez, conexión Bluetooth, soporte de vibración, detección de presión en los gatillos y mucho más (dejaré el enlace a la página del proyecto por si quieren ver todas sus ventajas, además de algunas características no disponibles).

**¿Por qué xpadneo y no otro?**

Mi mayor problema surgió con los juegos de Nintendo Switch, ya que al tratar de jugar con este tipo de controles, los botones dejaban de funcionar correctamente y por si fuera poco, el ratón quedaba bloqueado porque las palancas del control eran detectadas como ratón. Después de probar varios, en serio, varios controladores diferentes, xpadneo fue la aguja del pajar. Me anda perfectamente y al conectar el control al PC, genera un efecto de vibración para que sepas que la conexión fue exitosa.

Desde este pequeño rinconcito de tecnología, este pingüino quiere agradecer profundamente a [atar-axis](https://github.com/atar-axis), quien es el responsable del desarrollo de xpadneo. Les dejo aquí también un enlace con el que pueden [apoyar al desarrollo con un café](https://ko-fi.com/kakra).

En fin, dejemos ya la palabrería y disfrutemos del resultado. ¡No olvides compartir! 🐧
