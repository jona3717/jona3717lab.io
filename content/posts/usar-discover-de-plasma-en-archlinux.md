---
title: "Usar Discover De Plasma en Archlinux"
date: 2022-04-10T12:14:23-03:00
draft: false
tags:
    - linux
    - gnu
    - gnu linux
    - archlinux
    - tricks
    - tips
    - plasma
    - discover
    - kde
    - how to
---
{{< image src="https://cdn.kde.org/screenshots/plasma-discover/plasma-discover.png" style="border-radius: 3px;" >}}

## Introducción

En ocasiones me he visto en la situación en la que debo pedirle a alguien, que instale algún paquete en mi ordenador mientras estoy haciendo otra cosa. El problema en esto, es que lejos de ahorrarme tiempo me tomaba más tiempo por el hecho de no tener un gestor de paquetes o una tienda de software con interfaz gráfica. Por ello decidí usar una para esas ocasiones.

Gestores de paquetes con interfaz gráfica, los hay para todos los gustos, entre ellos tenemos a [pamac](https://gitlab.manjaro.org/applications/pamac) y a [octopi](https://github.com/aarnt/octopi), que son los más conocidos, sin embargo, en este post hablaremos de la tienda de aplicaciones de Plasma, Discover. Si han leído mis posts con anterioridad, sabrán que soy un usuario de Plasma y de Archlinux, por lo que esto responde a ¿qué mejor que usar una aplicación que se integre completamente con el entorno gráfico?

Antes de empezar, quiero aclarar, que Discover *no es un gestor de paquetes*, Discover es una tienda de aplicaciones que utiliza los metadatos de los paquetes para ofrecérnoslos. Por lo tanto, si quieres tener un catálogo más amplio de software disponible en Discover, es recomendable que uses Flatpak para algunos paquetes que podían no mostrarse pese a estar en los repositorios oficiales de Archlinux. Personalmente, prefiero no usar ninguno de esos repositorios de terceros, pero lo dejo aquí y a criterio de cada uno, porque tal vez le den más uso que yo a esta tienda de software. Bien dejemos el bla, bla, bla y vayamos a lo que vinimos. 

## Instalación 

1. Abrimos una terminal e instalamos Discover.
```
sudo pacman -S discover
```
2. Abrimos Discover y verificamos que todo funcione correctamente.
3. Si no te muestra aplicaciones, probablemente falten un par de paquetes.
4. Instalamos appstream-qt y archlinux-appstream-data.
```
sudo pacman -S appstream-qt archlinux-appstream-data
```

Ahora todo debería funcionar correctamente. Si no encuentras algún paquete en concreto, posiblemente sea porque el paquete no cuenta con los metadatos que requiere Discover para ser mostrado, por ello, como dije líneas arriba, deberás usar Flatpak o Snap, o mejor aún, usar la terminal.

No olvides compartir. 🐧
