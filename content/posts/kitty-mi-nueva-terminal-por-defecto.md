---
title: "Kitty Mi Nueva Terminal Por Defecto"
date: 2021-04-18T15:31:01-03:00
draft: false
tags:
  - linux
  - gnu
  - gnu linux
  - terminal
  - consola
  - console
  - linux tools
  - emulador
---

{{< image src="/images/kitty.png" >}}

Uso la terminal a diario y prácticamente para todo. Hasta hace una semana usaba Alacritty, una terminal impulsada por GPU. Realmente estaba muy a gusto y no tenía queja alguna con respecto a la terminal; Alacritty me da velocidad y simplicidad. Hasta que un compañero en un grupo de Telegram, recomendó Kitty. No dijo por qué, solo lo recomendó.
Al cabo de unos días, estuve leyendo sobre Kitty e instalándola en mi ordenador. Ahora mis impresiones:

## Instalación y configuración

Instalar esta aplicacion no le va a suponer un gran reto a nadie. Basta con descargarla desde la tienda de aplicaciones de la distribución o instalarla desde la terminal. Lo más seguro es que esté en los repositorios de tu distribución.

```
# Para Arch

sudo pacman -S kitty
```

Una vez instalada, podrás ejecutarla y tendrás una terminal como cualquier otra, pelada, sin ajustes visuales ni configuraciones, pero aquí es donde empieza lo divertido.

Para configurar Kitty, debemos editar el archivo de configuración: "~/.config/kitty/kitty.conf", no necesitas grandes conocimientos para esto y, además, tienes toda la documentación que necesitas en [la web del proyecto.](https://sw.kovidgoyal.net/kitty/#)

Entre leer y configurar todo, me tomé entre treinta y cuarenta y cinco minutos; una vez hecho eso quedaba empezar a probar.

## Características interesantes

Lo primero que quise probar fueron las pestañas y los "multi-ventanas". Con los atajos de teclado es realmente sencillo navegar entre layouts y pestañas. Redimensionar el espacio de cada ventana se hace muy sencillo gracias a una guía que te indica con qué teclas se redimenciona cada lado. Con estas características quedé más que satisfecho.

{{< image src="/images/kitty2.png" style="border-radius: 3px;" >}}

Luego probé lo que me hizo decidirme por cambiar de Alacritty a Kitty. Los kittens. Son addons para la terminal que permiten al usuario tener herramientas como visor de imagenes en la propia terminal, un visor de múltiples archivos (muy útil para comparar), un panel que muestra la salida de un comando o de la ejecución de una aplicación determinada (kitten mi favorito), etc. En fin, pequeños addons que cumplen determinadas tareas, pero ¿qué si busco un kitten que haga una tarea que no existe? Para eso, podemos desarrollar uno propio.

Gracias a la documentación, podemos crear nuestros propios kittens de una manera simple, por lo que si no encontramos el kitten con la funcionalidad que queremos, podemos hocernos uno. Sin embargo, siempre puedes encontrar los desarrollados por otros usuarios e ir probando si alguno te es útil.

## Conclusión

Kitten es una terminal que utiliza la GPU para el renderizado con addons (kittens) que hacen de este emulador de terminal una aplicación muy completa. Encontré una terminal potente, veloz, simple y además, con complementos que mejoran la productividad. De por sí, las "multiventanas" resultan muy útiles, pero los addons lo hacen todo mucho más interesante.

Esta entrada no es un tutorial para instalar o configurar, como ya te habrás dado cuenta. Es más mi opinión personal sobre esta aplicación y mis primeras impresiones al usarla. Espero que te anime a probarla y si lo haces, cuéntame qué te pareció.

No olvides compartir.
