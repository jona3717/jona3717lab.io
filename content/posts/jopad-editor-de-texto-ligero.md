---
title: "Jopad Un Editor De Texto Simple y Ligero"
date: 2020-07-18T18:06:36-03:00
draft: false
toc: false
images:
tags:
  - aplicacion
  - application
  - app
  - gnu/linux
  - linux
  - editor
  - texto
  - editor de texto
  - text editor
  - ligero
  - simple
  - lightweight
---

{{< image src="/images/jopad-about.png" style="border-radius: 3px;" >}}

**Sobre Jopad**

Es un editor de texto plano sin mucha ciencia. Permite visualizar, editar y modificar archivos de texto y posee las funciones básicas que todo editor de texto tiene. La característica de este editor, es que por tener como único propósito, el trabajar con archivos de texto plano, es un editor ligero, que no consume muchos recursos del ordenador y anda muy bien en equipos con bajos recursos.

**Especificaciones técnicas**

Jopad está desarrollado en C++, para la interfaz gráfica se ha utilizado el framework Qt5. Posee una licencia GNU GPL v3 por lo que es de software libre.

**Sobre el desarrollo**
{{< image src="/images/jopad-file.png" style="border-radius: 3px;" >}}

Actualmente no cuento con un ordenador con muy buenas características para desarrollar, es tanto asį que al ejecutar Kate, el editor de texto desarrollado por KDE, se traba. Hace unos días tuve ese problema con el editor de texto y fastidiado decidí desarrollar uno que cumpla con mostrarme archivos de texto. Así nace Jopad.
{{< image src="/images/jopad-edit.png" style="border-radius: 3px;" >}}

Espero que puedas probar la aplicación y me cuentes sobre tu experiencia con ella. Iré mejorando este editor añadiendo funciones y mejorando detalles. Para instalarla solo debes ir al repositorio en Gitlab, ahí están las instrucciones. [Click aquí.](https://gitlab.com/jona3717/jopad/)
{{< image src="/images/jopad-help.png" style="border-radius: 3px;" >}}

No olvides que puedes contactarme por cualquiera de mis redes, a las que puedes acceder desde el panel al lado derecho o enviándome un email a jona3717@protonmail.com, hasta otra oportunidad.

No olvides compartir.
