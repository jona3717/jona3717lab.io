---
title: "Funciones lambda en Python"
date: 2020-07-18T01:59:35-03:00
draft: false
toc: false
images: 
tags:
  - python
  - funciones
  - lambda
  - programacion
---

{{< image src="/images/python-banner.jpg" style="border-radius: 3px;" >}}

### ¿Qué son?

Son funciones que tienen una expresión simple y que no tienen nombre. Estas funciones de una sola línea sirven para utilizarse sobre la marcha, veamos un ejemplo que puedes ejecutar. 

Creamos una función lambda y la almacenamos en una variable llamada "func".

```
# Código

func = lambda x : x * 2

print(func(2))
```
```
#Salida de ejecución
> 4
```

[Ejemplo en Replit, aquí.](https://repl.it/@jonaxirinz/Expresiones-Lambda)

Lo que hará nuestra función es tomar el argumento que le pasemos y multiplicarlo por 2.

Para poder ver el resultado de ejecutar nuestra función, debemos pasarla por un «print» ya que lo que hace una función lambda es devolver el resultado de la expresión que le hemos asignado. Esto nos lleva también a lo siguiente, el valor booleano de una función lambda, siempre va ha ser True, por el hecho de retornar algo, siempre.

Las expresiones lambda pueden tener cualquier número de argumentos, devolviendo siempre lo que resulte de la expresión. Para que lo entiendas mejor puedes ejecutar el siguiente código de ejemplo.

<iframe height="400px" width="100%" src="https://repl.it/@jonaxirinz/Lambda-Argumentos?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

Como puedes ver en el ejemplo anterior, la función tiene dos argumentos, que utiliza para realizar una operación y devuelve un resultado.

Las funciones lambda son de mucha ayuda cuando se quiere ejecutar algo en modo de función, pero no debemos abusar de ellas tratando de crear una función compleja en una sola línea. Estas funciones deben usarse en código que no va ha ser reutilizado, que no requiere sentencias ni múltiples operaciones.

No olvides compartir.
