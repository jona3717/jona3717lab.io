---
title: "¿Que Es Qt?"
date: 2020-07-18T18:18:21-03:00
draft: false
toc: false
images:
tags:
  - programacion
  - programming
  - framework
  - gui
  - interfaces
  - qt
  - c++
  - cpp
---

{{< image  src="/images/qt.png" style="border-radius: 3px;" >}}

Es un framework desarrollado en 1991 por los programadores noruegos Erick Eng y Haavard Nord. Es utilizado para desarrollar aplicaciones y programas con interfaz gráfica o sin ella. Está desarrollado en C++ y utiliza el mismo lenguaje para el desarrollo de software, aunque también puede usarse con otros lenguajes como Python importando las librerías correspondientes. Actualmente se encuentra en la versión 5.14 y cuenta con una licencia GPLv3, sin embargo también está la versión comercial.

**¿Qué aplicaciones o programas están desarrollados con Qt?**

Hay una gran cantidad de software desarrollado con Qt, sin embargo uno de los más grandes proyectos desarrollados con este framework, es sin duda [Plasma de KDE.](https://kde.org/plasma-desktop)

Uno de los entornos de escritorio para el sistema operativo GNU/Linux más utilizados a día de hoy.

{{< image src="/images/plasma.png" style="border-radius: 3px;" >}}

Además podemos encontrar software como Google Earth, navegadores, administradores de archivos, clientes de correo, editores de imágenes, herramientas gráficas, IDEs, reproductores de música y un largo etcétera. Si quieres conocer más sobre software desarrollado con Qt, te dejo esta [lista de Manjaro.](https://wiki.manjaro.org/index.php?title=List_of_Qt_Applications)

**¿Por qué te hablo de Qt?**

Si sigues el blog, seguramente sabrás de algunas de las aplicaciones que he desarrollado. La mayoría de éstas, han sido desarrolladas con Qt y debo decirte que la experiencia durante el desasrrollo ha sido completamente satisfactoria y, aunque no soy un experto en el framework, con el poco conocimiento que he adquirido puedo decir que es uno de los frameworks más completos que hay.

**¿Aún no te convences?**

Qt es multiplataforma, esto quiere decir que puedes utilizarlo en WIndows, Linux o MacOS. Además las aplicaciones que desarrolles no se limitan al escritorio, sino también a dispositivos con Android. A que ahora se puso más iteresante.

Si quieres descargar y probar este framework, te dejo el [enlace a la web.](https://www.qt.io/download-qt-installer)

Si deseas que haga un tutorial de instalación, puedes ir a la [encuesta](https://t.me/Jona3717Canal/390) que dejaré en el canal de Telegram y votar.

No olvides compartir.
