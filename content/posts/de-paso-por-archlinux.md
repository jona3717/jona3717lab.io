---
title: "De Paso Por Archlinux"
date: 2020-08-01T22:34:18-03:00
draft: false
toc: false
images:
header:
  image: "https://jona3717.gitlab.io/images/archlinux-plasma.jpg"
tags:
  - de paso por archlinux
  - arch
  - arch linux
  - archlinux
  - gnu
  - linux
  - gnu/linux
  - distros
  - distro
  - rolling release
  - rolling
---

{{< image src="/images/archlinux-plasma.jpg" style="border-radius: 3px;" >}}

Ya sea sobre la propia Archlinux o alguna de sus derivadas, esta serie de artículos que comienza con este, está pensada para dar a conocer algunas de las características de esta distro, aplicaciones útiles, algún que otro "truco" para optimizar, personalización y más. No me considero un usuario experimentado de Arch, pero me parece que lo poco que sé puede servirle a muchos otros que como yo alguna vez, se están introduciendo en esta distribución.

Me parece que esta serie no puede iniciar, sin antes comentar un poco sobre la distribución, así que veamos algunos puntos importantes.

## ¿Qué es Archlinux?

Es una distribución GNU/Linux de lanzamiento continuo, esto quiere decir, que se encuentra en constante actualización. Si eres usuario de Windows, deja que te calme diciéndote que no, no te va a actualizar el sistema al apagarlo ni lo hará sin tu propia intervención, vamos, se actualiza tal y como lo hace cualquier otra distro, solo cuando tú se lo pides. Esta distribución es una de las grandes conocidas y no es para menos por su método de instalación por comandos. Sin embargo hoy en día tenemos instaladores para esta distribución que nos facilitan el proceso, como Anarchy; además, tenemos también las distribuciones que derivan de esta que como mínimo traen un instalador "semi gráfico" (del tipo ncurses para los entendidos) que te quitan todo problema. Una de las derivadas más conocidas es Manjaro, que tiene distintos sabores (Entornos de escritorio) y que cuenta con algunos paquetes (aplicaciones y programas) precargados para que no heches en falta nada cuando termines de instalar tu sistema.

## Las ventajas de tener Archlinux (Según mi experiencia)

- Si te tomaste el trabajo de instalar Arch puro y duro, siguiendo alguna guía o la wiki, la mayor ventaja es que aprendiste y mucho, por lo menos sobre la arquitectura de una distribución, los sistemas de archivos, el uso básico de la terminal, montaje y desmontaje de dispositivos, particiones y un largo etcétera. 

- Al ser una distribución rolling release, tu sistema siempre va a estar actualizado y, aunque no siempre lo más nuevo es lo mejor, sé que hay muchos que prefieren ir por la vida con software actualizado o probando los nuevos lanzamientos, yo soy parte de ese grupo. 

- Hay ocasiones en las que uno se ve envuelto en un problema con el ordenador, si estás familiarizado con las distros, sabes de las comunidades, esos espacios maravillosos en los que pueden ayudarte on cualquier problema que tengas. Pero claro, no todo es color de rosa, porque así como las hay buenas, puedes toparte con muchas comunidades tóxicas, que podrían desanimarte en menos de lo que tardaste en instalar tu distro. Por eso hay que dedicar algo de tiempo para encontrar buenas comunidades activas. Pero eso no es todo, Archlinux cuenta con una wiki muy completa, prácticamente todo lo que puedas necesitar, lo vas a encontrar aquí, tal vez el único contra con el que te podrás encontrar es que la mayor parte de la wiki está en inglés. Pero como dije, la documentación para esta distro es basta.

- Esta es una de las ventajas de Arch que más me agradan, la paquetería. En los repositorios oficiales se puede encontrar casi todos los programas o aplicaciones que podamos necesitar, y digo casi porque puede que alguna aplicación no esté en los repositorios oficiales, pero tranquilo, lo más seguro es que la encuentres en el repositorio comunitario AUR. Un repositorio mantenido por la comunidad en el que tienes miles de paquetes disponibles para que no extrañes nada. Con esto puedes olvidarte de agregar repositorios extra para instalar alguna aplicación, como sucede con otras distros. No es que sea malo, pero considero menos tedioso, más práctico y más seguro no tener que agregar repositorios externos, pero como siempre digo: "Lo mejor es tratar de evitar el uso de AUR, y limitarse a usar los repositorios oficiales."

Desde luego que hay más ventajas, pero prefiero dejarlo en esas cuatro, para que puedas descubrir por ti mismo las demás, así no te quito la diversion.

## Desventajas de usar Archlinux

- Como decía líneas arriba, no todo es color de rosa y para iniciar con las desventajas, la primera es también el método de instalación que utiliza Arch, es cierto que hay alternativas, pero entra en esta lista por el tiempo que puede tomarte instalar y dejar a punto tu sistema operativo.

- Esto es algo de lo que afortunadamente, desde el día que empecé a usar Arch hasta hoy no he sufrido, sin embargo, se dice por ahí que por ser de lanzamiento continuo, puede que se actualice algún paquete y este por tener algún conflicto te rompa el sistema. Como he dicho, desde que empecé con Arch hasta ahora, no me ha pasado, pero no dejo de contarlo, solo por si las dudas.

- Como dije, casi todos los paquetes puedes encontrarlos en el repositorio oficial y si no es ahí, en el AUR; no obstante, puede que alguna aplicación, ni si quiera esté en el AUR, por lo que tendrás que descargar el código fuente y compilarla por ti mismo, que no es tarea dificil, pero si estás empezando seguro que eso te supone un dolor de cabeza. Personalmente, los paquetes que he tenido que compilar son contados y no son aplicaciones de usuario básico, sino que han sido aplicaciones para desarrolladores o semejantes, así que tampoco hay mucho de qué preocuparse por acá.

## Conclusiones

Como puedes apreciar, desde mi opinión personal Arch es una distro robusta, práctica y ligera. Pero como es bien sabido "Para gustos, los colores", así que no hay mejor forma de saber si es para ti o no, que probándola.
Desde este pequeño rinconcito te animo a probar esta distro y me cuentes sobre tu experiencia y tu opinión. Si ya la probaste, entonces no estaría mal saber como te fue. Te dejo una captura de mi Archlinux con Plasma e i3 Gaps.

{{< image src="/images/Archlinux-Jona3717.png" style="border-radius: 3px;" >}}

Como se puede apreciar, conseguí el script de pacman. Pero no lo escribí yo, lo he tomado de [este repositorio](https://github.com/da-edra/dotfiles)

**No olvides compartir.**
