---
title: "Juega mejor y aumenta el rendimiento de tus juegos con Gamemode"
date: 2022-04-17T15:41:06-03:00
draft: false
toc: false
images:
tags:
  - gaming
  - juegos
  - linux
  - gnu
  - gnu/linux
  - linuxgaming
  - linux gaming
  - videojuegos
  - gamemode
  - gamemoderun
  - tips
  - apps
  - aplicaciones
  - linux tips
---

{{< image src="/images/gamer-girl.jpg" style="border-radius: 3px;" >}}
Photo by RODNAE Productions from Pexels

## Introducción

Me encantan los videojuegos; no recuerdo ya cuantos tengo o cuantos he jugado, pero no siempre he tenido la posibilidad de jugar a todos los juegos que he querido. Muchas veces me perdí de jugar algún titulo por no contar con los requisitos adecuados en el ordenador o simplemente no continuaba jugando por el mal rendimiento que tenia a causa de los pocos recursos de mi ordenador. Para ocasiones similares, existen boosters o potenciadores que permiten mejorar de cierta forma el rendimiento de los juegos. Aquí es donde entra Gamemode, para no entrar en tecnicismos, es un potenciador que permite aplicar distintas optimizaciones al sistema operativo mientras se ejecuta un juego. Esta pequeña pero poderosa aplicación se encuentra disponible para: Ubuntu, Debian, Solus, Arch, Gentoo, Fedora, OpenSUSE, Mageia y otras distribuciones GNU/Linux.

Antes de continuar, debo comentarte lo siguiente. Si tienes un ordenador con recursos de medios altos a altos, no notarás mucha diferencia, sin embargo, si por el contrario cuentas con un ordenador de gama media, media baja, entonces podrás notar la mejora en tus juegos.

## instalación

Para instalar Gamemode en tu distribución, puedes hacerlo con cualquiera de estos comandos dependiendo de la misma:

Debian / Ubuntu:

sudo apt-get install gamemode

Archlinux / Arch based:

sudo pacman -S gamemode

Nota: En algunas distribuciones basadas en Debian, he tenido problemas con gamemode, se instala correctamente, pero no me funciona del mismo modo, por lo que la mejor opción en esos casos,es instalarlo manualmente.

### Instalación manual:

```
git clone https://github.com/FeralInteractive/gamemode.git
cd gamemode
git checkout 1.6.1 # omit to build the master branch
./bootstrap.sh
```
### Desinstalar Gamemode:

```
systemctl --user stop gamemoded.service
ninja uninstall -C builddir
```

## Como usar Gamemode
Existen dos métodos para utilizar Gamemode:
### Con cualquier juego:
```
gamemoderun ./juego
```
### En Steam:
En la lista de juegos, selecciona el juego, click derecho y selecciona la opción propiedades, en la ventada de propiedades ve a parámetros de lanzamiento e ingresa el comando:
```
gamemoderun %command%
```
Cierra la ventana y ejecuta tu juego.

Seria fabuloso saber como te anduvo gamemode y conocer tu experiencia, así que si te animas, recuerda que puedes dejar un comentario en la parte de abajo del post.

No olvides compartir. 
