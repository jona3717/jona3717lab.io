---
title: "Struct y enum en Rust: ¿Qué son y en qué se diferencian?"
date: 2022-09-28T02:06:38-03:00
draft: false
tags:
  - desarrollo
  - programación
  - development
  - rust
  - rust-lang
  - struct
  - enum
---

{{< image src="/images/rust-pet.gif" style="border-radius: 3px;" >}}

## ¿Qué son struct y enum?

Entre los distintos tipos de datos que podemos encontrar en Rust, tenemos estos dos también llamados tipos de datos personalizados (*custom data types*). Como su nombre nos indica, nos permiten crear tipos de datos más complejos. Para hacerte una idea, puedes pensar en ellos como colecciones de datos, similares a los diccionarios en Python. Veamos un ejemplo de cada tipo para conocer su sintaxis.

```rust
// Definimos una enumeración (enum)
enum CarModel {
    Sedan,
    Convertible,
    Hatchback,
    Sunroof(bool),
}

// Definimos una estructura (struct)
struct Car {
    Color: &str,
    Model: CarModel,
    Year: i32,
}
```

Los elementos que vemos dentro de la enum CarModel se llaman variantes (*variants*), y los que están en la struct Car, se llaman campos (*fields*). Como puedes ver, son muy similares, veamos qué tanto.

Si quieres conocer más sobre estructuras y enumeraciones puedes ver la documentación de Rust en español, [aquí](https://doc-rust--lang-org.translate.goog/book/title-page.html?_x_tr_sl=en&_x_tr_tl=es&_x_tr_hl=es-419&_x_tr_pto=wapp).

## ¿En qué se diferencian?

Según lo que dice la documentación de Rust, la principal diferencia es que las enumeraciones permiten definir un tipo de dato enumerando sus posibles variantes y las estructuras permiten agrupar campos y datos relacionados.

Tomando de ejemplo el código anterior, nuestra estructura es el conjunto de datos que describen un auto, y nuestra enumeración, enumera (valga la redundancia) los modelos de un auto.
De esta forma, podemos decir que un auto, puede pertenecer a un modelo a la vez, pero puede tener distintas características (campos) como: color, modelo y año.

Ahora bien, si picamos un poco de código y jugamos con estos dos tipos de datos, encontraremos que podemos hacer las mismas cosas con uno y con otro, entonces vuelve la pregunta: ¿Qué diferencia hay entre los dos?
Veamos:

### Enumeración:

- Basta con inicializar una de sus variantes.
- El tamaño de una enumeración es el tamaño de la variante más grande más el identificador de variante. Todas sus variantes tienen el mismo tamaño

### Estructura:

- Se debe asignar el valor a cada uno de sus campos.
- El tamaño de la estructura es la suma de los tamaños de sus campos y su contenido.

Veamos un ejemplo de estas diferencias:

```rust
struct MyStruct {
    Element1: u32,
    Element2: u8,
    Element3: u32
}

enum MyEnum {
    ElementA(u32),
    ElementB(u8),
    ElementC(u32)
}
```

El tamaño de la enumeración es 64 bits. 32 de la variante de mayor tamaño y 32 de su identificador.

El tamaño de la estructura es de 96 bits. Cada campo a 32, por lo que hay 24 bits de relleno, ya que uno de los campos es de 8 bits.

## Conclusión

Aunque estos dos tipos de datos sean parecidos, no son lo mismo. Ahora sabemos que una enumeración ocupa menos espacio en la memoria que una estructura; de este modo y conociendo sus diferencias ahora sabremos en qué situaciones utilizar uno u otro.

### Referencias:

- [https://stackoverflow.com/questions/56830274/difference-between-struct-and-enum](https://stackoverflow.com/questions/56830274/difference-between-struct-and-enum)
- [https://doc.rust-lang.org/book/](https://doc.rust-lang.org/book/)

No olvides compartir. 🐧
