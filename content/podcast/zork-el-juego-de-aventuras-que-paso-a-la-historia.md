---
title: "Zork El Juego De Aventuras Que Paso a La Historia"
date: 2023-05-23T17:46:04-03:00
draft: false
toc: false
images:
tags:
  - juego
  - videojuego
  - game
  - zork
  - retro
  - juego de texto
  - juego de rol
  - rol
---

{{< image src="/images/zork.jpg" style="border-radius: 3px;" >}}
##

{{< archive 3-Zork-el-juego-de-aventura-que-paso-a-la-historia.mp3 >}}

Este episodio también se encuentra disponible como entrada en el blog.

¿Alguna vez oíste hablar de Zork? Hoy te contaré sobre el legendario juego de aventuras. Recuerda que si lo prefieres, también puedes encontrar este episodio en el blog (https://unpinguino.net/blog/) como una entrada.

¡No olvides compartir! 🐧

Gracias a [Mauricio Díaz García](https://juegosdetexto.wordpress.com/) por facilitar el juego en español.

[👉Zork - Juego completo en español👈](https://iplayif.com/?story=http%3A//media.textadventures.co.uk/games/GQLrNEIS8k26ddJ2nco6ag/Zork.gblorb)

[Música](http://jamen.do/t/965255)
