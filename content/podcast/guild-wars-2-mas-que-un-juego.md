---
title: "Guild Wars 2 más que un juego"
date: 2023-03-14T11:00:15-03:00
draft: false
toc: false
images:
tags:
  - games
  - videojuegos
  - opinion
  - reseña
  - juegos
  - guild wars 2
  - GW2
---

{{< image src="/images/guild-wars-2.jpg" style="border-radius: 3px;" >}}

Este episodio también se encuentra disponible en video en el canal Jona3717 Play.

Aventura, guerras, descubrimiento, escenarios asombrosos. Todo eso y más espera en Guild Wars 2, el juego que no es solo un juego.

{{< archive 1-Mas-que-un-juego-Guild-Wars-2.mp3 >}}
