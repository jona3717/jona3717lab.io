---
title: "¿Qué pasó con Telltale Games?"
date: 2023-06-02T00:44:24-03:00
draft: false
toc: false
images:
tags:
  - telltale games
  - telltale
  - games
  - juegos
  - compañia videojuegos
  - videojuegos
---

{{< image src="/images/telltalegames.jpg" style="border-radius: 3px;" >}}
Imagen por [Caballero de la tormenta](https://hiepsibaotap.com/wp-content/uploads/2020/04/telltale-games-generacion-xbox.jpg)
##

{{< archive 5-Que-paso-con-telltale-games.mp3 >}}


¿Conoces a la compañía responsable de títulos como The Walking Dead, Tales from the Borderlands o The Wolf Among Us? Hoy te contaré un poco sobre Telltale Games, su caída y su resurgir.
#
Recuerda que también puedes encontrar este episodio en las siguientes plataformas.


🔴 YouTube: [Pingüino Gamer](https://youtube.com/@unpinguinogamer)

🗒 Blog: [El blog de un Pingüino](https://unpinguino.net/)

Música: Jamendo (https://jamendo.com/)
