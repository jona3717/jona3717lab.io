---
title: "Revelaciones Kde Plasma 6"
date: 2023-05-14T00:24:04-03:00
draft: false
toc: false
images:
tags:
  - gnulinux
  - gnu
  - linux
  - kde
  - kdeplasma
  - kde plasma
  - plasma
  - plasma6
  - noticias
  - noticia
  - podcast
---

{{< image src="/images/plasma-portada.png" style="border-radius: 3px;" >}}

Este episodio también se encuentra disponible como entrada en el blog.

Cada vez más cerca y ahora ¡Ya tenemos novedades! Te cuento de algunas de las nuevas características de KDE Plasma 6.

[Blog de Nate Graham](https://pointieststick.com/2023/05/11/plasma-6-better-defaults/?ref=news.itsfoss.com)

{{< archive 2-Revelaciones-KDE-Plasma-6.mp3 >}}
