---
title: "Juegos Indie y por qué debes probarlos si aún no lo has hecho"
date: 2023-05-27T22:37:47-03:00
draft: false
toc: false
images:
tags:
  - juegos
  - indie
  - juegos indie
  - games
  - indie games
  - game
  - videojuegos
---


{{< image src="/images/juegos-indie.jpg" style="border-radius: 3px;" >}}
Imagen por [The Zeus Player](https://www.instagram.com/the_zeus_player/)
##

{{< archive 4-Que-son-los-juegos-indie-y-por-que-debes-probarlos.mp3 >}}


¿Has oído hablar acerca de los juegos indie? Hoy te hablo un poco de ellos y te dejo algunas recomendaciones de juegos que te harán pasar horas de entretenimiento.
#
Recuerda que también puedes encontrar este episodio en las siguientes plataformas.


🔴 YouTube: [Pingüino Gamer](https://youtube.com/@unpinguinogamer)

🗒 Blog: [El blog de un Pingüino](https://unpinguino.net/)

Música: Jamendo (https://jamendo.com/)
