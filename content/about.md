+++
title = "Acerca de"
date = "2022-09-07"
aliases = ["about-unpinguino.net","about-us","about-jona3717","contact"]
[ author ]
  name = "Jona3717"
+++

Esta web no es un proyecto con fines de lucro, por ello no encontrarás ningún tipo de publicidad.

**Unpinguino.net** ha ido creciendo y evolucionando poco a poco desde sus inicios como "Días Geek" sin dejar de lado su esencia.
Aquí encontrarás tutoriales, reseñas y mucho más. Todo el contenido que se encuentra en nuestro proyecto está hecho con el único deseo de compartir y está pensado para la comunidad. Así que espero que hayas encontrado en este humilde espacio algo grato de leer. 

**Si deseas apoyar a este proyecto con una donación, puedes hacerlo por PayPal:** 
[Aquí](https://www.paypal.me/jona3717)

Puedes encontrar el código del proyecto en [GitLab](https://gitlab.com/jona3717/jona3717.gitlab.io).
